Rails.application.routes.draw do
  resources :options
  resources :requests
  resources :availabilities
  resources :nannies
  root             'static_pages#home'
  get 'help'    => 'static_pages#help'
  post   'help'   => 'static_pages#help'
  get 'about'   => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
  get 'admin_controll'    => 'static_pages#admin_controll'
  get 'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  resources :users

  # Links für Header
  get 'nannies'    => 'nannies#index'
  get 'admin_optimize'  => 'static_pages#admin_optimize'
  get 'requests'    => 'requests#index'
  get 'availabilities'    => 'availabilities#index'
  get 'newnanny' => 'users#newnanny'
  get 'allclients' => 'users#indexclient'
  get 'nanniesedit'    => 'nannies#edit'
  get 'optimize'    => 'requests#optimize'
  get 'simulator'   => 'static_pages#simulator'


  # Sonstige
  get 'noaccess'    => 'static_pages#noaccess'
  get 'requestsdatatable'  => 'requests#requestsdatatable'
  get 'addtimes'    => 'availabilities#addtimes'
  get 'gamstest'    => 'options#gamstest'
  get 'read_solution'    => 'application#read_solution'
  get 'rate_nanny'    => 'nannies#rate_nanny'


  # Links für Simulator
  get 'randomrequest'   => 'simulators#randomrequest'
  get 'randompast'   => 'simulators#randompast'
  get 'tenrandompasts'   => 'simulators#tenrandompasts'
  get 'tenrandomrequests'   => 'simulators#tenrandomrequests'
  get 'randomclient'   => 'simulators#randomclient'
  get 'randomnanny'   => 'simulators#randomnanny'

end
