module ApplicationHelper
  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "EN"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  def text_with_badge(text, badge_value=nil)
    badge = content_tag :span, badge_value, class: 'badge'
    text = raw "#{text} #{badge}" if badge_value
    return text
  end

end
