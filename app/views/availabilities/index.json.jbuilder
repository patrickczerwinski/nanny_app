json.array!(@availabilities) do |availability|
  json.extract! availability, :id, :nannyname, :date, :morning, :noon, :afternoon, :evening
  json.url availability_url(availability, format: :json)
end
