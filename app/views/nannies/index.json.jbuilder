json.array!(@nannies) do |nanny|
  json.extract! nanny, :id, :name, :sex, :age, :education, :driverlicense, :language_german, :language_english, :language_spanish, :language_polish, :price, :rating
  json.url nanny_url(nanny, format: :json)
end
