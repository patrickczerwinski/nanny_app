json.extract! @nanny, :id, :name, :sex, :age, :education, :driverlicense, :language_german, :language_english, :language_spanish, :language_polish, :price, :rating, :created_at, :updated_at
