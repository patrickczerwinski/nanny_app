json.array!(@requests) do |request|
  json.extract! request, :id, :name, :sex, :age, :education, :driverlicense, :language_german, :language_english, :language_spanish, :language_polish, :willingness_to_pay, :rating, :date, :time, :minage, :maxage, :assigned_nanny
  json.url request_url(request, format: :json)
end
