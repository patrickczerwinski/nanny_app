class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper
  before_action :lastlogin


  def read_solution
    if (File.exist?("solution.txt"))

      # Ergebnisse aus der Solution-Datein in die Datenbank eintragen

      fil=File.open("solution.txt", "r")
      line=fil.readline
      sa=line.split(" ")
      @solutionZFW=sa[1]
      fil.each { |line| # printf(f,line)
        sa=line.split(" ; ")
        sa0=sa[0].delete " \n"    # Nanny-Name
        sa1=sa[1].delete " \n"    # Kundenname + Auftrags-ID
        sa2=sa[2].delete " \n"    # Datum
        sa3=sa[3].delete " \n"    # Zeit

        splitforid=sa1.split("-")
        # Teilt den String wiederum, so dass splitforid einen Array mit
        # [Kundenname, Auftrags-ID] ergibt.
        # Damit kann nun der korrekte Auftrag zugeordnet werden.

        als = Request.find_by_id(splitforid)
        als.assigned_nanny = sa0
        als.status = 1
        als.save


        # Die Verfügbarkeiten aktualisiert
        ava = Availability.find_by_nannyname_and_date(sa0, sa2)

        if sa3 == "morning"
          ava.morning = 2
        elsif sa3 == "noon"
          ava.noon = 2
        elsif sa3 == "afternoon"
          ava.afternoon = 2
        elsif sa3 == "evening"
          ava.evening = 2
        end
        ava.save
      }

      # Den Status von den nicht zugeordneten Aufträgen ändern (also von 0 zu 2)

      @unassignedrequests = Request.where(status: 0, assigned_nanny: nil)

      @unassignedrequests.each { |req|
        req.status = 2
        req.save
      }

      flash.now[:success] = "Ergebnisse erfolgreich übertragen!"

      fil.close

    else
      # Wenn keine solution.txt Datei vorhanden ist
      flash.now[:success] = "Noch kein Ergebniss berechnet!"

    end

    render 'static_pages/admin_optimize'

  end


  def lastlogin
    # Diese Funktion prüft regelmäßig, wann der letzte login war. Beim ersten Login an einem Tag
    # wird der Status aller vergangener und nicht zugeordneter Aufträge in "3" geändert.
    if $lastlogin == nil
      $lastlogin = Date.today - 1
    end

    if $lastlogin.to_date.past?
      $lastlogin = Date.today

      # Zunächst wird der Status der Aufträge geändert
      @oldrequests = Request.where(assigned_nanny: nil)

      @oldrequests.each do |oldr|

        if oldr.date.to_date.past?

          oldr.status = 3
          oldr.save
        end

      end

      # Prüfen welche Verfügbarkeiten in der Vergangenheit liegen und diese löschen
      @allavas = Availability.all
      @allavas.each do |ava|

        if ava.date.to_date.past?
          ava.delete
        else

        end
      end


    end
  end


end
