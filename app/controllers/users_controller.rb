class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update]
 # before_action :admin_user,     only: :destroy
  before_action :checkloggedin, except: [:new, :newnanny, :create]
  before_action :checkloggedin2, only: [:new, :newnanny, :create]

  def checkloggedin2
    if logged_in?
      redirect_to current_user
    end
  end

  def checkloggedin
    if logged_in?
    else
      flash[:danger] = "Kein Zugriff. Bitte einloggen."
      redirect_to login_path
    end
  end

  def index
    @users = User.paginate(page: params[:page])
  end

  def indexclient
    @users = User.where(role: "client").paginate(page: params[:page])
    @client = true
  end

  def show
    @user = User.find(params[:id])
  end

  # GET /orts/newnanny
  # GET /orts/newnanny.json
  def newnanny
    @user = User.new


    respond_to do |format|
      format.html # newnanny.html.erb
      format.json { render json: @user }
    end
  end


  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)    # Not the final implementation!
    if @user.save
      log_in @user
      flash[:success] = "Ihr Account wurde erfolgreich erstellt. Willkommen!"
      redirect_to @user
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

  # DELETE /nannies/1
  # DELETE /nannies/1.json
  def destroy
    if admin?
      admincheck = true
    end

    name = User.find(params[:id]).name
    User.find(params[:id]).destroy

    if admincheck == true
    redirect_to users_path
    flash[:success] = "Nutzer " + name + " erfolgreich gelöscht!"

    else
      log_out
      redirect_to root_path
      end
  end


  private

  def user_params
    params.require(:user).permit(:name, :email, :password,
                                 :password_confirmation, :city, :street, :role, :fullname)
  end

  # Before filters

  # Confirms a logged-in user.
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end


  # Confirms the correct user.
  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user)
  end

  # Confirms an admin user.
  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end
end
