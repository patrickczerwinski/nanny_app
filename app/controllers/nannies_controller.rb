class NanniesController < ApplicationController
  before_action :set_nanny, only: [:show, :edit, :update, :destroy]
  before_action :checkloggedin
  before_action :checkrole, only: [:index, :new, :edit, :update, :create, :destroy]
  before_action :checkadmin, only: [:new]
  before_action :correct_user, only: [:edit, :update, :destroy]

  def correct_user
    if nanny?
      @user = User.find_by_name(@nanny.name)
      redirect_to(noaccess_path) unless current_user?(@user)
    end
  end

  def checkadmin
    # Admin soll keine neuen Nannys erstellen
    if admin?
      redirect_to noaccess_path
    end
  end

  def checkrole
    if nanny? or admin?
    else
      redirect_to noaccess_path
    end
  end

  def checkloggedin
    if logged_in?
    else
      flash[:error] = "Kein Zugriff. Bitte einloggen."
      redirect_to login_path
    end
  end

  # GET /nannies
  # GET /nannies.json
  def index
    if admin?
    @nannies = Nanny.all
    else
      redirect_to noaccess_path
      end
  end

  # GET /nannies/1
  # GET /nannies/1.json
  def show
  end

  # GET /nannies/new
  def new


    # Es wird geprüft, ob der aktuelle Nutzer bereits Nanny-Daten eingegeben hat.
    # Wenn nicht, dass werden neue erstellt, wenn doch, wird er zum Editieren weitergeleitet.

    currentusername = current_user.name
    nannythere = Nanny.find_by_sql("SELECT * FROM nannies WHERE name = '#{currentusername}'")


    if nannythere.any? == true
      redirect_to nanniesedit_path
      else
    @nanny = Nanny.new
      end
  end

  # GET /nannies/1/edit
  def edit
  end

  # POST /nannies
  # POST /nannies.json
  def create
    @nanny = Nanny.new(nanny_params)

    respond_to do |format|
      if @nanny.save

        format.html { redirect_to availabilities_path}
        format.json { render :show, status: :created, location: @nanny }
        flash[:success] = "Nanny-Daten erfolgreich aktualisiert."
      else
        format.html { render :new }
        format.json { render json: @nanny.errors, status: :unprocessable_entity }
      end
    end
  end



  # PATCH/PUT /nannies/1
  # PATCH/PUT /nannies/1.json
  def update
    respond_to do |format|
      if @nanny.update(nanny_params)
        format.html { redirect_to edit_nanny_path(@nanny) }
        format.json { render :show, status: :ok, location: @nanny }
        flash[:success] = "Nanny-Daten erfolgreich aktualisiert."
      else
        format.html { render :edit }
        format.json { render json: @nanny.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nannies/1
  # DELETE /nannies/1.json
  def destroy
    @nanny.destroy
    respond_to do |format|
      format.html { redirect_to root_path }
      flash[:success] = "Nanny-Daten erfolgreich gelöscht."

      format.json { head :no_content }
    end
  end

  def rate_nanny
    nannyname = params[:nannyname]
    newrating = params[:newrating]
    requestid = params[:requestid]


    # Zunächst das bestehende Rating ziehen
    thenanny = Nanny.find_by_name(nannyname)
    if thenanny.nil?
      flash[:warning] = "Der Nanny-Account existiert nicht mehr. Eine Bewertung ist daher nicht mehr möglich."

    else

    currentrating = thenanny.rating
    numberofratings = thenanny.ratingscount


    # Falls keine Bewertungen vorhanden sind, sollen die nil Werte zu 5 bzw 0 werden
    if currentrating == nil
      currentrating = 5
    end
    if numberofratings == nil
      numberofratings = 0
    end

    newnumberofratings = numberofratings + 1
    wholenewrating = (currentrating.to_i * numberofratings + newrating.to_i ) / newnumberofratings

    # Das neue Ergebnis eintrage
    thenanny.rating = wholenewrating.to_i
    thenanny.ratingscount =+1

    # In den Request ratingdone = true setzen und ratingvalue setzen
    therequest = Request.find(requestid)
    therequest.ratingdone = true
    therequest.ratingvalue = newrating

    therequest.save
    thenanny.save

    flash[:success] = "Vielen Dank für die Bewertung! :)"

    end
    redirect_to :back

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nanny

      if nanny?

        @nanny = Nanny.find_by_name(current_user.name)

      else admin?
      @nanny = Nanny.find(params[:id])
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def nanny_params
      params.require(:nanny).permit(:name, :sex, :age, :education, :driverlicense, :language_german, :language_english, :language_spanish, :language_polish, :price, :rating, :ratingscount, :minage, :maxage)
    end
end
