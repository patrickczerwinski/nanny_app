class StaticPagesController < ApplicationController
  def home
    # Damit unterschiedliche Startseiten erscheinen

  if logged_in?
      if admin?
        redirect_to admin_controll_path
      else
        redirect_to requests_path
      end
    end
  end

  def help

  end

  def about
  end

  def contact
  end

  def admin_controll
    if admin?
    else
      redirect_to noaccess_path
    end
  end

  def admin_optimize
    if admin?

      if Option.find(1) == nil
        Option.create(id: 1, optionname: "gamspath", value: "Hier GAMS-Pfad eintragen.. ")
      end

    else
      redirect_to noaccess_path
    end
  end

  def simulator
    if admin?
    else
      redirect_to noaccess_path
    end
  end

  def requestsdatatable
    if logged_in? == false
        redirect_to noaccess_path
    end
  end

  def noaccess
  end


end
