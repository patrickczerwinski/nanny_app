class RequestsController < ApplicationController
  before_action :set_request, only: [:show, :edit, :update, :destroy]
  before_action :checkloggedin
  before_action :checknanny, only: [:new, :edit, :update, :create, :destroy]
  before_action :correct_user_request, only: [:show, :edit]
  before_action :checkstatus, only: [:edit, :destroy]

  def checkstatus
    if @request.status == 0 or @request.status == 2
    else
      redirect_to noaccess_path
      flash[:danger] = "Vergangene oder zugeordnete Aufträge können nicht bearbeitet werden."

    end
  end


  def correct_user_request
    if admin?
    elsif client?
      @user = User.find_by_name(@request.name)
      redirect_to(noaccess_path) unless current_user?(@user)
    elsif nanny?
      @user = User.find_by_name(@request.assigned_nanny)
      redirect_to(noaccess_path) unless current_user?(@user)
    end
  end


  def checknanny
    if nanny?
      redirect_to noaccess_path
    else
    end
  end


  def checkloggedin
    if logged_in?
    else
      flash[:danger] = "Kein Zugriff. Bitte einloggen."
      redirect_to login_path
    end
  end

  # GET /requests
  # GET /requests.json
  def index


    if admin?
      @requests0 = Request.all

    else
      if client?
        @requests0 = Request.where(name: current_user.name)

      else
        if nanny?
          # Zuerst prüfen ob die Nanny-Daten bereits hinzugefügt wurden, wenn nicht dann muss dies zunächst geschehen
          nannythere = Nanny.find_by_sql("SELECT * FROM nannies WHERE name = '#{current_user.name}'")


          if nannythere.any? == false
            flash[:warning] = "Hinweis: Bevor die Aufträge eingesehen werden können, müssen die Nanny Daten eingetragen werden."

            redirect_to new_nanny_path
            @requests0 = Request.where(assigned_nanny: current_user.name)

          else
            @requests0 = Request.where(assigned_nanny: current_user.name)

          end
        end
      end
    end

    # Den korrekten View auch auswählen

    @whichview = params[:whichview]
    if @whichview == nil
      # Definiert den "Standard-View"
      @whichview = "onlynew"
    end

    if @whichview == "onlyold"
      @requests1 = @requests0.where(date: (Time.now.midnight - 100.year)..Time.now.midnight)
    elsif @whichview == "onlynew"
      @requests1 = @requests0.where(date: Time.now.midnight..(Time.now.midnight + 99.year))
    elsif @whichview == "lastweek"
      @requests1 = @requests0.where(date: (Time.now.midnight - 1.week)..Time.now.midnight)
    else
      @requests1 = @requests0
    end

    # Ordnet die Anfragen
    @requests = @requests1.sort_by { |request| request.date }.reverse


  end


  # GET /requests/1
  # GET /requests/1.json
  def show
  end

  # GET /requests/new
  def new
    @request = Request.new
  end

  # GET /requests/1/edit
  def edit
  end

  # POST /requests
  # POST /requests.json
  def create
    @request = Request.new(request_params)

    respond_to do |format|
      if @request.save
        format.html { redirect_to @request }
        format.json { render :show, status: :created, location: @request }
        flash[:success] = "Anfrage erfolgreich erstellt."

      else
        format.html { render :new }
        format.json { render json: @request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /requests/1
  # PATCH/PUT /requests/1.json
  def update
    respond_to do |format|
      if @request.update(request_params)
        format.html { redirect_to @request }
        format.json { render :show, status: :ok, location: @request }
        flash[:success] = "Anfrage erfolgreich aktualisiert."

      else
        format.html { render :edit }
        format.json { render json: @request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /requests/1
  # DELETE /requests/1.json
  def destroy
    @request.destroy
    respond_to do |format|
      format.html { redirect_to requests_url }
      format.json { head :no_content }
      flash[:success] = "Anfrage erfolgreich gelöscht."
    end
  end

  def requestsdatatable
    if admin?
      @requests0 = Request.all
    else
      if client?
        @requests0 = Request.where(name: current_user.name)
      else
        if nanny?
          @requests0 = Request.where(assigned_nanny: current_user.name)
        end
      end
    end

    @requests = @requests0

  end


  def optimize

    # Zunächst wird die Include Datei aufgebaut

    if File.exist?("include.inc")
      File.delete("include.inc")
    end

    # INCLUDE DATEI AUFBAUEN  -------------------------

    f=File.new("include.inc", "w")

    # SETS
    printf(f, "set i / \n")
    @nannies = Nanny.all
    @nannies.each { |nan| printf(f, nan.name + "\n") }
    printf(f, "/" + "\n\n")

    printf(f, "j / \n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |req| printf(f, req.name + "-" + req.id.to_s + "\n") }
    printf(f, "/" + "\n\n")

    printf(f, "t / \n")

    @i = 0
    (1..7).each { |t| printf(f, (Date.today + @i).to_s + "\n")
    @i+=1 }

    printf(f, "/" + "\n\n")

    printf(f, "s / \n")
    printf(f, "morning" + "\n")
    printf(f, "noon" + "\n")
    printf(f, "afternoon" + "\n")
    printf(f, "evening" + "\n")
    printf(f, "/;" + "\n\n")

    # PARAMETER

    #Provisionssatz (ist fix)

    printf(f, "Parameter\n  r /\n")
    printf(f, "5" + "\n")
    printf(f, "/" + "\n\n")

    # Zeiten

    printf(f, "Parameter\n  tV(i,t,s) /\n")

    # Verfügbarkeiten der Nannys

    @nannies = Nanny.all
    @nannies.each { |l|

      @availabilites = Availability.where(nannyname: l.name)
      @availabilites.each { |k|

        if k.morning == 1
          printf(f, l.name + "." + k.date + "." + "morning" + "     1" "\n")
        end

        if k.noon == 1
          printf(f, l.name + "." + k.date + "." + "noon" + "     1" + "\n")
        end

        if k.afternoon == 1
          printf(f, l.name + "." + k.date + "." + "afternoon" + "     1"+ "\n")
        end

        if k.evening == 1
          printf(f, l.name + "." + k.date + "." + "evening" + "     1" + "\n")
        end
      }
    }
    printf(f, "/" + "\n\n")


    # Gewünschte Zeiten der Anfragen

    printf(f, "Parameter\n  tG(j,t,s) /\n")

    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |l|
      printf(f, l.name + "-" + l.id.to_s + "." + l.date + "." + l.time + "     1" + "\n")
    }
    printf(f, "/" + "\n\n")


    # Führerscheine -------------

    # Führerscheine der Nannys

    printf(f, "Parameter\n  FV(i) /\n")
    @nannies = Nanny.all
    @nannies.each { |nan|
      if nan.driverlicense == true
        printf(f, nan.name + "   " + "1" + "\n")
      else
        printf(f, nan.name + "   " + "0" + "\n")
      end
    }
    printf(f, "/" + "\n\n")

    # Führerscheinnachfragen bei den Anfragen

    printf(f, "Parameter\n  FG(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |req|
      if req.driverlicense == true
        printf(f, req.name + "-" + req.id.to_s + "   " + "1" + "\n")
      else
        printf(f, req.name + "-" + req.id.to_s + "   " + "0" + "\n")
      end
    }
    printf(f, "/" + "\n\n")

    printf(f, "Parameter\n  BF(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |req|
      if req.driverlicense == false
        printf(f, req.name + "-" + req.id.to_s + "   " + "1" + "\n")
      else
        printf(f, req.name + "-" + req.id.to_s + "   " + "0" + "\n")
      end
    }
    printf(f, "/" + "\n\n")


    # Geschlechter   --------------------

    # Weiblich
    printf(f, "Parameter\n  GWV(i) /\n")
    @nannies = Nanny.all
    @nannies.each { |l|
      if l.sex == "w"
        printf(f, l.name + "   " + "1" + "\n")
      else
        printf(f, l.name + "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    printf(f, "Parameter\n  GWG(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |l|
      if l.sex == "w"
        printf(f, l.name + "-" + l.id.to_s + "   " + "1" + "\n")
      else
        printf(f, l.name + "-" + l.id.to_s + "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    # Männlich
    printf(f, "Parameter\n  GMV(i) /\n")
    @nannies = Nanny.all
    @nannies.each { |l|
      if l.sex == "m"
        printf(f, l.name + "   " + "1" + "\n")
      else
        printf(f, l.name + "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    printf(f, "Parameter\n  GMG(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |l|
      if l.sex == "m"
        printf(f, l.name + "-" + l.id.to_s+ "   " + "1" + "\n")
      else
        printf(f, l.name + "-" + l.id.to_s+ "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    # Egal
    printf(f, "Parameter\n  bG(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |l|
      if l.sex == "egal" or l.sex == nil
        printf(f, l.name + "-" + l.id.to_s+ "   " + "1" + "\n")
      else
        printf(f, l.name + "-" + l.id.to_s+ "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")


    # Ausbildung

    printf(f, "Parameter\n  aV(i) /\n")
    @nannies = Nanny.all
    @nannies.each { |l|
      if l.education == true
        printf(f, l.name + "   " + "1" + "\n")
      else
        printf(f, l.name + "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    printf(f, "Parameter\n  aG(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |l|
      if l.education == true
        printf(f, l.name + "-" + l.id.to_s + "   " + "1" + "\n")
      else
        printf(f, l.name + "-" + l.id.to_s + "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    # Egal
    printf(f, "Parameter\n  bA(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |l|
      if l.education == false
        printf(f, l.name + "-" + l.id.to_s+ "   " + "1" + "\n")
      else
        printf(f, l.name + "-" + l.id.to_s+ "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    # Sprachen

    # Deutsch
    printf(f, "Parameter\n  DSV(i) /\n")
    @nannies = Nanny.all
    @nannies.each { |l|
      if l.language_german == true
        printf(f, l.name + "   " + "1" + "\n")
      else
        printf(f, l.name + "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    printf(f, "Parameter\n  DSG(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |l|
      if l.language_german == true
        printf(f, l.name + "-" + l.id.to_s + "   " + "1" + "\n")
      else
        printf(f, l.name + "-" + l.id.to_s + "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    # Deutsch-Egal
    printf(f, "Parameter\n  bDS(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |l|
      if l.language_german == false
        printf(f, l.name + "-" + l.id.to_s+ "   " + "1" + "\n")
      else
        printf(f, l.name + "-" + l.id.to_s+ "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    #Englisch

    printf(f, "Parameter\n  ESV(i) /\n")
    @nannies = Nanny.all
    @nannies.each { |l|
      if l.language_english == true
        printf(f, l.name + "   " + "1" + "\n")
      else
        printf(f, l.name + "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    printf(f, "Parameter\n  ESG(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |l|
      if l.language_english == true
        printf(f, l.name + "-" + l.id.to_s + "   " + "1" + "\n")
      else
        printf(f, l.name + "-" + l.id.to_s + "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    # Englisch-Egal
    printf(f, "Parameter\n  bES(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |l|
      if l.language_english == false
        printf(f, l.name + "-" + l.id.to_s+ "   " + "1" + "\n")
      else
        printf(f, l.name + "-" + l.id.to_s+ "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    #Polnisch

    printf(f, "Parameter\n  PSV(i) /\n")
    @nannies = Nanny.all
    @nannies.each { |l|
      if l.language_polish == true
        printf(f, l.name + "   " + "1" + "\n")
      else
        printf(f, l.name + "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    printf(f, "Parameter\n  PSG(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |l|
      if l.language_polish == true
        printf(f, l.name + "-" + l.id.to_s + "   " + "1" + "\n")
      else
        printf(f, l.name + "-" + l.id.to_s + "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    # Polnisch-Egal
    printf(f, "Parameter\n  bPS(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |l|
      if l.language_polish == false
        printf(f, l.name + "-" + l.id.to_s+ "   " + "1" + "\n")
      else
        printf(f, l.name + "-" + l.id.to_s+ "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    #Spanisch

    printf(f, "Parameter\n  SSV(i) /\n")
    @nannies = Nanny.all
    @nannies.each { |l|
      if l.language_spanish == true
        printf(f, l.name + "   " + "1" + "\n")
      else
        printf(f, l.name + "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    printf(f, "Parameter\n  SSG(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |l|
      if l.language_spanish == true
        printf(f, l.name + "-" + l.id.to_s + "   " + "1" + "\n")
      else
        printf(f, l.name + "-" + l.id.to_s + "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    # Spanisch-Egal
    printf(f, "Parameter\n  bSS(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |l|
      if l.language_spanish == false
        printf(f, l.name + "-" + l.id.to_s+ "   " + "1" + "\n")
      else
        printf(f, l.name + "-" + l.id.to_s+ "   " + "0" + "\n")

      end
    }
    printf(f, "/" + "\n\n")

    # Zahlungsbereitschaft und Preis--------------------

    printf(f, "Parameter\n  P(i) /\n")
    @nannies = Nanny.all
    @nannies.each { |l|
      printf(f, l.name + "   " + l.price.to_s + "\n")
    }
    printf(f, "/" + "\n\n")

    printf(f, "Parameter\n  ZB(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |l|
      if l.willingness_to_pay == nil
        printf(f, l.name + "-" + l.id.to_s + "    9999" + "\n")
      else
        printf(f, l.name + "-" + l.id.to_s + "   " + l.willingness_to_pay.to_s + "\n")
      end
    }
    printf(f, "/" + "\n\n")

    # Bewertung

    printf(f, "Parameter\n  qV(i) /\n")
    @nannies = Nanny.all
    @nannies.each { |l|
      if l.rating == nil or l.rating == 0.0
        printf(f, l.name + "   " + "0" + "\n")

      else
        printf(f, l.name + "   " + l.rating.to_s + "\n")
      end
    }
    printf(f, "/" + "\n\n")

    printf(f, "Parameter\n  qG(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |l|
      if l.rating == nil or l.rating == 0.0
        printf(f, l.name + "-" + l.id.to_s + "   " + "0" + "\n")

      else
        printf(f, l.name + "-" + l.id.to_s + "   " + l.rating.to_s + "\n")
      end
    }
    printf(f, "/" + "\n\n")

    # Alter

    printf(f, "Parameter\n  ageNanny(i) /\n")
    @nannies = Nanny.all
    @nannies.each { |l|
      if l.age == nil or l.age == 0
        printf(f, l.name + "   0" + "\n")

      else
        printf(f, l.name + "   " + l.age.to_s + "\n")
      end
    }
    printf(f, "/" + "\n\n")

    printf(f, "Parameter\n  ageNmax(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |l|
      if l.maxage == nil or l.maxage == 0.0
        printf(f, l.name + "-" + l.id.to_s + "   " + "1000" + "\n")
      else
        printf(f, l.name + "-" + l.id.to_s + "   " + l.maxage.to_s + "\n")
      end
    }
    printf(f, "/" + "\n\n")

    printf(f, "Parameter\n  ageNmin(j) /\n")
    @requests = Request.where("status = ? OR status = ?", 0, 2)
    @requests.each { |l|
      if l.minage == nil or l.minage == 0.0
        printf(f, l.name + "-" + l.id.to_s + "   0" + "\n")

      else
        printf(f, l.name + "-" + l.id.to_s + "   " + l.minage.to_s + "\n")
      end
    }
    printf(f, "/" + "\n\n")

    f.close

    # INCLUDE DATEI FERTIG

    # Das GAMS Modell starten

    flash.now[:success] = "Die Optimierung wurde erfolgreich gestartet!"

    gamspath = Option.find(1).value
    system gamspath + " modell"

    flash.now[:success] = "Die Optimierung wurde erfolgreich abgeschlossen!"

    render 'static_pages/admin_optimize'
  end


  private
  # Use callbacks to share common setup or constraints between actions.
  def set_request
    @request = Request.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def request_params
    params.require(:request).permit(:name, :sex, :age, :education, :driverlicense, :language_german, :language_english, :language_spanish, :language_polish, :willingness_to_pay, :rating, :date, :time, :minage, :maxage, :assigned_nanny, :ratingdone, :ratingvalue, :status)
  end
end
