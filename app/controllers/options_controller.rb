class OptionsController < ApplicationController
  before_action :set_option, only: [ :update, :destroy]
  before_action :checkadmin

  def checkadmin
    if admin? == false
      redirect_to noaccess_path
    end
  end

  # POST /options
  # POST /options.json
  def create
    @option = Option.new(option_params)

    respond_to do |format|
      if @option.save
        format.html { redirect_to admin_optimize_path, notice: 'Option was successfully created.' }
        format.json { render :show, status: :created, location: @option }
      else
        format.html { render :new }
        format.json { render json: @option.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /options/1
  # PATCH/PUT /options/1.json
  def update
    respond_to do |format|
      if @option.update(option_params)
        format.html { redirect_to admin_optimize_path }
        flash[:success] = "GAMS-Pfad erfolgreich aktualisiert."
        format.json { render :show, status: :ok, location: @option }
      else
        format.html { render :edit }
        format.json { render json: @option.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /options/1
  # DELETE /options/1.json
  def destroy
    @option.destroy
    respond_to do |format|
      format.html { redirect_to admin_optimize_path, notice: 'Option erfolgreich gelöscht.' }
      format.json { head :no_content }
    end
  end

  def gamstest

    if File.exist?("gamstest-result.txt")
      File.delete("gamstest-result.txt")
    end

    @gamspath = Option.find(1)

    system @gamspath.value + " gamstest"

    sleep(1)

    if File.exist?("gamstest-result.txt")

      flash[:success] = "GAMS-Pfad richtig."
    else
      flash[:danger] = "GAMS-Pfad falsch."

    end

      render 'static_pages/admin_optimize'

  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_option
      @option = Option.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def option_params
      params.require(:option).permit(:optionname, :value)
    end
end
