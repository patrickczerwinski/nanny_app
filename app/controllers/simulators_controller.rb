class SimulatorsController < ApplicationController

  def randomrequest

    # Zufällige Auswahl eines Kunden

    client = User.where(role: "client").sample

    sex = ["w", "m"].sample
    education = false
    driverlicense = false
    languageG = false
    languageE = false
    languageS = false
    languageP = false
    willpay = rand(45..60)
    reqday  =  Date.today + rand(0..6)
    rating =  rand(1..5).to_i
    time =     ["morning", "noon", "afternoon", "evening"].sample
    minage = rand(18..20).to_i
    maxage = rand(40..65).to_i

    Request.create!(name: client.name, sex: sex, education: education, driverlicense: driverlicense,
                    language_german: languageG, language_english: languageE, language_spanish: languageS,
                    language_polish: languageP, willingness_to_pay: willpay, date: reqday, rating: rating,
                    time: time, minage: minage, maxage: maxage, status: "0")

    flash[:info] = "Es wurde ein zufälliger Auftrag für den Kunden " + client.name + " erstellt."
    redirect_to simulator_path
  end


  def randomclient

    city = Faker::Address.city
    street = Faker::Address.street_address
    fullname = Faker::Name.first_name + " " + Faker::Name.last_name
    email = Faker::Internet.free_email
    name = fullname.delete(' ').downcase

    User.create!(name: name, email: email,
                 password: "123456", password_confirmation: "123456",
                 role: "client", city: city, street: street,
                 fullname: fullname)

    flash[:info] = "Es wurde ein zufälliger Kunde mit dem Namen " + name + " erstellt."
    redirect_to simulator_path

  end

  def randomnanny

    # Zuerst den User

    city = Faker::Address.city
    street = Faker::Address.street_address
    fullname = Faker::Name.first_name + " " + Faker::Name.last_name
    email = Faker::Internet.safe_email
    name = fullname.delete(' ').downcase

    User.create!(name: name, email: email,
                 password: "123456", password_confirmation: "123456",
                 role: "nanny", city: city, street: street,
                 fullname: fullname)

    # Dann die Nanny

    sex = ["w", "m"].sample
    education = false
    driverlicense = false
    languageG = false
    languageE = false
    languageS = false
    languageP = false
    price = rand(8..25)
    rating =  rand(1..5).to_i
    ratingscount =  rand(1..30)
    age = rand(18..40).to_i

    Nanny.create!(name: name, sex: sex, age: age, education: education, driverlicense: driverlicense,
                  language_german: languageG, language_english: languageE, language_spanish: languageS, language_polish: languageP,
                  price: price, rating: rating, ratingscount: ratingscount)


    # Schließlich noch die Verfügbarkeiten

      7.times do |n|

        availdate = Date.today + n
        morny =  [0,1,1,1].sample
        noony =  [0,1,1,1].sample
        afternoony = [0,1,1,1].sample
        eveningy =  [0,1,1,1].sample

        Availability.create!(nannyname: name, date: availdate, morning: morny, noon: noony, afternoon: afternoony,
                             evening: eveningy)
      end

    flash[:info] = "Es wurde eine zufällige Nanny mit dem Namen " + name + " erstellt."
    redirect_to simulator_path

  end

  def tenrandomrequests

    10.times do
    client = User.where(role: "client").sample

    sex = ["w", "m"].sample
    education = false
    driverlicense = false
    languageG = false
    languageE = false
    languageS = false
    languageP = false
    willpay = rand(45..60)
    reqday  =  Date.today + rand(0..6)
    rating =  rand(1..4).to_i
    time =     ["morning", "noon", "afternoon", "evening"].sample
    minage = rand(18..20).to_i
    maxage = rand(40..65).to_i

    Request.create!(name: client.name, sex: sex, education: education, driverlicense: driverlicense,
                    language_german: languageG, language_english: languageE, language_spanish: languageS,
                    language_polish: languageP, willingness_to_pay: willpay, date: reqday, rating: rating,
                    time: time, minage: minage, maxage: maxage, status: "0")
    end

    flash[:info] = "Es wurden 10 zufällige Aufträge für verschiedene Kunden erstellt."
    redirect_to simulator_path
  end


  def randompast
    # Erstellt einen vergangenen Auftrag

    client = User.where(role: "client").sample

    sex = ["w", "m"].sample
    education = false
    driverlicense = false
    languageG = false
    languageE = false
    languageS = false
    languageP = false
    willpay = rand(45..60)
    reqday  =  Date.today - rand(1..60)
    rating =  rand(1..5).to_i
    time =     ["morning", "noon", "afternoon", "evening"].sample
    minage = rand(18..20).to_i
    maxage = rand(40..65).to_i

    # Wurde der Auftrag zugeteilt?
    assigned = [0,1,1,1,1].sample

    if assigned == 1
      ananny = Nanny.all.sample
      assigned_nanny = ananny.name
      status = 1
      # Schon bewertet?
      rated = rand(0..1)
      if rated == 1
        ratingdone = true
      ratingvalue = rand(1..5)
      else
        ratingdone = nil
        ratingvalue = nil
        end
    else
      assigned_nanny = nil
      ratingdone = nil
      ratingvalue = nil
      status = 3
    end

    Request.create!(name: client.name, sex: sex, education: education, driverlicense: driverlicense,
                    language_german: languageG, language_english: languageE, language_spanish: languageS,
                    language_polish: languageP, willingness_to_pay: willpay, date: reqday, rating: rating,
                    time: time, minage: minage, maxage: maxage, status: "0", assigned_nanny: assigned_nanny,
                    assigned_nanny: assigned_nanny, ratingdone: ratingdone, ratingvalue: ratingvalue, status: status)

    flash[:info] = "Es wurde ein zufälliger vergangener Auftrag für den Kunden " + client.name + " erstellt."
    redirect_to simulator_path

  end

  def tenrandompasts
    # Erstellt 10 vergangene Aufträge

    10.times do

    client = User.where(role: "client").sample
    sex = ["w", "m"].sample
    education = false
    driverlicense = false
    languageG = false
    languageE = false
    languageS = false
    languageP = false
    willpay = rand(45..60)
    reqday  =  Date.today - rand(1..60)
    rating =  rand(1..5).to_i
    time =     ["morning", "noon", "afternoon", "evening"].sample
    minage = rand(18..20).to_i
    maxage = rand(40..65).to_i

    # Wurde der Auftrag zugeteilt?
    assigned = [0,1,1,1,1].sample

    if assigned == 1
      ananny = Nanny.all.sample
      assigned_nanny = ananny.name
      status = 1
      # Schon bewertet?
      rated = rand(0..1)
      if rated == 1
        ratingdone = true
        ratingvalue = rand(1..5)
      else
        ratingdone = nil
        ratingvalue = nil
      end
    else
      assigned_nanny = nil
      ratingdone = nil
      ratingvalue = nil
      status = 3
    end

    Request.create!(name: client.name, sex: sex, education: education, driverlicense: driverlicense,
                    language_german: languageG, language_english: languageE, language_spanish: languageS,
                    language_polish: languageP, willingness_to_pay: willpay, date: reqday, rating: rating,
                    time: time, minage: minage, maxage: maxage, status: "0", assigned_nanny: assigned_nanny,
                    assigned_nanny: assigned_nanny, ratingdone: ratingdone, ratingvalue: ratingvalue, status: status)
    end

    flash[:info] = "Es wurden 10 zufällige vergangene Aufträge für verschiedene Kunden erstellt."
    redirect_to simulator_path

  end
end

