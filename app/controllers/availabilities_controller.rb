class AvailabilitiesController < ApplicationController
  before_action :set_availability, only: [:show, :edit, :update, :destroy]
  before_action :checkloggedin
  before_action :checkrole
  before_action :correct_user, only: [:show, :edit, :update, :destroy]

  def correct_user
    if admin? == false
      @ava = Availability.find(params[:id])
      @user = User.find_by_name(@ava.nannyname)
      redirect_to(noaccess_path) unless current_user?(@user)
    end
  end

  def checkrole
    if nanny? or admin?
    else
      redirect_to noaccess_path
    end
  end

  def checkloggedin
    if logged_in?
    else
      flash[:error] = "Kein Zugriff. Bitte einloggen."
      redirect_to login_path
    end
  end


  # GET /availabilities
  # GET /availabilities.json
  def index

    currentusername = current_user.name


    if admin?

      currentusername = params[:nannyname]
      nannythere = Nanny.find_by_sql("SELECT * FROM nannies WHERE name = '#{currentusername}'")

      if currentusername == nil
        redirect_to nannies_path
        flash[:warning] = "Bitte Nanny auswählen."


        # Prüfen ob die Nanny vorhanden ist

      elsif nannythere.any? == false

        flash[:warning] = "Diese Nanny ist nicht vorhanden."

        redirect_to nannies_path
      else
        $i = 0
        $num = 7

        begin
          currentnewdate = Date.today + $i

          nannydate = Availability.find_by_sql("SELECT * FROM availabilities WHERE (nannyname = '#{currentusername}' AND date = '#{currentnewdate}')")

          if nannydate.any? == false
            @availabilities = Availability.create!(nannyname: currentusername, date: currentnewdate, morning: false,
                                                   noon: false, afternoon: false, evening: false)
          end


          $i +=1

        end while $i < $num

        @availabilities = Availability.where(nannyname: currentusername)
      end

    else

      # Zuerst prüfen ob die Nanny-Daten bereits hinzugefügt wurden, wenn nicht dann muss dies zunächst geschehen
      nannythere = Nanny.find_by_sql("SELECT * FROM nannies WHERE name = '#{currentusername}'")

    end


    if nanny?
      if nannythere.any? == false
        flash[:warning] = "Hinweis: Bevor die Verfügbarkeiten bearbeitet werden können, müssen die Nanny Daten eingetragen werden."

        redirect_to new_nanny_path

      else

        $i = 0
        $num = 7

        begin
          currentnewdate = Date.today + $i

          nannydate = Availability.find_by_sql("SELECT * FROM availabilities WHERE (nannyname = '#{currentusername}' AND date = '#{currentnewdate}')")

          if nannydate.any? == false
            @availabilities = Availability.create!(nannyname: currentusername, date: currentnewdate, morning: false, noon: false, afternoon: false, evening: false)
          end


          $i +=1

        end while $i < $num

        @availabilities = Availability.where(nannyname: currentusername)
      end

    end


  end

  # GET /availabilities/1
  # GET /availabilities/1.json
  def show
    redirect_to noaccess_path

  end

  # GET /availabilities/new
  def new
    #@availability = Availability.new
    redirect_to noaccess_path
  end

  # GET /availabilities/1/edit
  def edit

  end

  # POST /availabilities
  # POST /availabilities.json
  def create
    @availability = Availability.new(availability_params)

    respond_to do |format|
      if @availability.save
        format.html { redirect_to @availability, notice: 'Availability was successfully created.' }
        format.json { render :show, status: :created, location: @availability }
      else
        format.html { render :new }
        format.json { render json: @availability.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /availabilities/1
  # PATCH/PUT /availabilities/1.json
  def update
    respond_to do |format|
      if @availability.update(availability_params)
        format.html { redirect_to edit_availability_path(@availability) }
        flash[:success] = "Verfügbarkeiten wurden erfolgreich aktualisiert."

        format.json { render availabilities_path, status: :ok, location: @availability }
      else
        format.html { render availabilities_path }
        format.json { render json: @availability.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /availabilities/1
  # DELETE /availabilities/1.json
  def destroy
    @availability.destroy
    respond_to do |format|
      format.html { redirect_to availabilities_url, notice: 'Availability was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def addtimes

    nannyname = params[:nannyname]
    time = params[:time]
    delete = true if params[:m] == "delete"
    if delete == nil
      a = 1
    else
      a = 0
    end

    @availabilities = Availability.where(nannyname: nannyname)

    @availabilities.each do |availability|


      if time == "morning"
        if availability.morning != 2
          availability.morning = a

        end
      end
      if time == "noon"
        if availability.noon != 2
          availability.noon = a
        end
      end
      if time == "afternoon"
        if availability.afternoon != 2
          availability.afternoon = a
        end
      end
      if time == "evening"
        if availability.evening != 2
          availability.evening = a
        end
      end

      availability.save


    end

    redirect_to :back

  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_availability
    @availability = Availability.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def availability_params
    params.require(:availability).permit(:nannyname, :date, :morning, :noon, :afternoon, :evening)
  end
end
