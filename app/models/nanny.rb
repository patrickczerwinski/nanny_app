class Nanny < ActiveRecord::Base
  validates :price, presence: true, :numericality => { :greater_than_or_equal_to => 0, :less_than_or_equal_to => 500}
  validates :age, :numericality => { :greater_than_or_equal_to => 0, :less_than_or_equal_to => 100}, allow_nil: true

  # self.primary_key = "name"
  has_many :availabilities, foreign_key: "nannyname", primary_key: "name",  :dependent => :destroy
  has_many :requests, foreign_key: "assigned_nanny", primary_key: "name"
  belongs_to :user


end
