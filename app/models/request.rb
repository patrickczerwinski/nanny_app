class Request < ActiveRecord::Base
  validates :date, presence: true
  validates :time, presence: true
  validates :willingness_to_pay, allow_nil: true, :numericality => { :greater_than_or_equal_to => 0, :less_than_or_equal_to => 500}
  validates :maxage, :numericality => { :greater_than_or_equal_to => 0, :less_than_or_equal_to => 100}, allow_nil: true
  validates :minage, :numericality => { :greater_than_or_equal_to => 0, :less_than_or_equal_to => 100}, allow_nil: true

  belongs_to :user
  belongs_to :nanny
end
