set i / 
nanny1
nanny2
nanny3
nanny4
nanny5
nanny6
nanny7
nanny8
nanny9
asdasd
/

j / 
client5-7
client9-20
client5-22
client25-331
client14-332
client17-333
client5-334
client24-335
client25-336
client22-337
client5-338
client9-339
client29-340
client21-341
/

t / 
2017-03-17
2017-03-18
2017-03-19
2017-03-20
2017-03-21
2017-03-22
2017-03-23
/

s / 
morning
noon
afternoon
evening
/;

Parameter
  r /
5
/

Parameter
  tV(i,t,s) /
nanny1.2017-03-17.noon     1
nanny1.2017-03-17.evening     1
nanny1.2017-03-18.morning     1
nanny1.2017-03-18.noon     1
nanny1.2017-03-18.afternoon     1
nanny1.2017-03-18.evening     1
nanny1.2017-03-19.morning     1
nanny1.2017-03-19.afternoon     1
nanny1.2017-03-20.afternoon     1
nanny1.2017-03-20.evening     1
nanny1.2017-03-21.morning     1
nanny1.2017-03-21.afternoon     1
nanny1.2017-03-21.evening     1
nanny2.2017-03-17.morning     1
nanny2.2017-03-17.noon     1
nanny2.2017-03-17.evening     1
nanny2.2017-03-18.morning     1
nanny2.2017-03-18.afternoon     1
nanny2.2017-03-19.noon     1
nanny2.2017-03-19.afternoon     1
nanny2.2017-03-19.evening     1
nanny2.2017-03-20.morning     1
nanny2.2017-03-20.afternoon     1
nanny2.2017-03-20.evening     1
nanny2.2017-03-21.morning     1
nanny2.2017-03-21.noon     1
nanny3.2017-03-17.noon     1
nanny3.2017-03-17.evening     1
nanny3.2017-03-18.morning     1
nanny3.2017-03-18.noon     1
nanny3.2017-03-19.afternoon     1
nanny3.2017-03-20.morning     1
nanny3.2017-03-20.afternoon     1
nanny3.2017-03-20.evening     1
nanny3.2017-03-21.noon     1
nanny3.2017-03-21.evening     1
nanny4.2017-03-17.noon     1
nanny4.2017-03-17.evening     1
nanny4.2017-03-18.afternoon     1
nanny4.2017-03-18.evening     1
nanny4.2017-03-19.morning     1
nanny4.2017-03-19.evening     1
nanny4.2017-03-20.noon     1
nanny4.2017-03-20.evening     1
nanny4.2017-03-21.noon     1
nanny4.2017-03-21.afternoon     1
nanny4.2017-03-21.evening     1
nanny5.2017-03-17.morning     1
nanny5.2017-03-17.noon     1
nanny5.2017-03-17.afternoon     1
nanny5.2017-03-17.evening     1
nanny5.2017-03-18.morning     1
nanny5.2017-03-18.noon     1
nanny5.2017-03-18.afternoon     1
nanny5.2017-03-18.evening     1
nanny5.2017-03-19.morning     1
nanny5.2017-03-19.noon     1
nanny5.2017-03-19.afternoon     1
nanny5.2017-03-19.evening     1
nanny5.2017-03-20.morning     1
nanny5.2017-03-20.afternoon     1
nanny5.2017-03-20.evening     1
nanny5.2017-03-21.afternoon     1
nanny6.2017-03-17.noon     1
nanny6.2017-03-17.afternoon     1
nanny6.2017-03-18.noon     1
nanny6.2017-03-18.afternoon     1
nanny6.2017-03-18.evening     1
nanny6.2017-03-19.morning     1
nanny6.2017-03-19.noon     1
nanny6.2017-03-19.afternoon     1
nanny6.2017-03-19.evening     1
nanny6.2017-03-20.morning     1
nanny6.2017-03-21.morning     1
nanny6.2017-03-21.noon     1
nanny6.2017-03-21.afternoon     1
nanny7.2017-03-17.noon     1
nanny7.2017-03-17.evening     1
nanny7.2017-03-18.morning     1
nanny7.2017-03-18.noon     1
nanny7.2017-03-18.afternoon     1
nanny7.2017-03-18.evening     1
nanny7.2017-03-19.morning     1
nanny7.2017-03-19.noon     1
nanny7.2017-03-19.afternoon     1
nanny7.2017-03-19.evening     1
nanny7.2017-03-20.morning     1
nanny7.2017-03-20.noon     1
nanny7.2017-03-21.afternoon     1
nanny7.2017-03-21.evening     1
nanny8.2017-03-17.morning     1
nanny8.2017-03-17.noon     1
nanny8.2017-03-17.evening     1
nanny8.2017-03-18.morning     1
nanny8.2017-03-18.noon     1
nanny8.2017-03-18.afternoon     1
nanny8.2017-03-18.evening     1
nanny8.2017-03-19.afternoon     1
nanny8.2017-03-20.noon     1
nanny8.2017-03-21.noon     1
nanny8.2017-03-21.afternoon     1
nanny8.2017-03-21.evening     1
nanny9.2017-03-17.morning     1
nanny9.2017-03-17.afternoon     1
nanny9.2017-03-17.evening     1
nanny9.2017-03-18.morning     1
nanny9.2017-03-18.afternoon     1
nanny9.2017-03-18.evening     1
nanny9.2017-03-19.morning     1
nanny9.2017-03-19.afternoon     1
nanny9.2017-03-19.evening     1
nanny9.2017-03-20.noon     1
nanny9.2017-03-20.afternoon     1
nanny9.2017-03-21.morning     1
nanny9.2017-03-21.noon     1
nanny9.2017-03-21.evening     1
/

Parameter
  tG(j,t,s) /
client5-7.2017-03-18.noon     1
client9-20.2017-03-20.evening     1
client5-22.2017-03-20.noon     1
client25-331.2017-03-19.evening     1
client14-332.2017-03-18.morning     1
client17-333.2017-03-21.noon     1
client5-334.2017-03-21.afternoon     1
client24-335.2017-03-22.afternoon     1
client25-336.2017-03-19.noon     1
client22-337.2017-03-20.morning     1
client5-338.2017-03-21.evening     1
client9-339.2017-03-21.noon     1
client29-340.2017-03-20.noon     1
client21-341.2017-03-18.afternoon     1
/

Parameter
  FV(i) /
nanny1   1
nanny2   1
nanny3   1
nanny4   0
nanny5   1
nanny6   1
nanny7   1
nanny8   1
nanny9   0
asdasd   0
/

Parameter
  FG(j) /
client5-7   1
client9-20   1
client5-22   0
client25-331   0
client14-332   0
client17-333   0
client5-334   0
client24-335   0
client25-336   0
client22-337   0
client5-338   0
client9-339   0
client29-340   0
client21-341   0
/

Parameter
  BF(j) /
client5-7   0
client9-20   0
client5-22   1
client25-331   1
client14-332   1
client17-333   1
client5-334   1
client24-335   1
client25-336   1
client22-337   1
client5-338   1
client9-339   1
client29-340   1
client21-341   1
/

Parameter
  GWV(i) /
nanny1   0
nanny2   0
nanny3   1
nanny4   1
nanny5   1
nanny6   0
nanny7   1
nanny8   1
nanny9   0
asdasd   0
/

Parameter
  GWG(j) /
client5-7   0
client9-20   0
client5-22   0
client25-331   1
client14-332   0
client17-333   0
client5-334   1
client24-335   1
client25-336   0
client22-337   1
client5-338   1
client9-339   0
client29-340   0
client21-341   0
/

Parameter
  GMV(i) /
nanny1   1
nanny2   1
nanny3   0
nanny4   0
nanny5   0
nanny6   1
nanny7   0
nanny8   0
nanny9   1
asdasd   0
/

Parameter
  GMG(j) /
client5-7   1
client9-20   1
client5-22   1
client25-331   0
client14-332   1
client17-333   1
client5-334   0
client24-335   0
client25-336   1
client22-337   0
client5-338   0
client9-339   1
client29-340   1
client21-341   1
/

Parameter
  bG(j) /
client5-7   0
client9-20   0
client5-22   0
client25-331   0
client14-332   0
client17-333   0
client5-334   0
client24-335   0
client25-336   0
client22-337   0
client5-338   0
client9-339   0
client29-340   0
client21-341   0
/

Parameter
  aV(i) /
nanny1   1
nanny2   1
nanny3   1
nanny4   1
nanny5   1
nanny6   1
nanny7   1
nanny8   1
nanny9   1
asdasd   0
/

Parameter
  aG(j) /
client5-7   0
client9-20   0
client5-22   0
client25-331   0
client14-332   0
client17-333   0
client5-334   0
client24-335   0
client25-336   0
client22-337   0
client5-338   0
client9-339   0
client29-340   0
client21-341   0
/

Parameter
  bA(j) /
client5-7   1
client9-20   1
client5-22   1
client25-331   1
client14-332   1
client17-333   1
client5-334   1
client24-335   1
client25-336   1
client22-337   1
client5-338   1
client9-339   1
client29-340   1
client21-341   1
/

Parameter
  DSV(i) /
nanny1   1
nanny2   1
nanny3   1
nanny4   1
nanny5   0
nanny6   1
nanny7   0
nanny8   1
nanny9   0
asdasd   0
/

Parameter
  DSG(j) /
client5-7   0
client9-20   0
client5-22   0
client25-331   0
client14-332   0
client17-333   0
client5-334   0
client24-335   0
client25-336   0
client22-337   0
client5-338   0
client9-339   0
client29-340   0
client21-341   0
/

Parameter
  bDS(j) /
client5-7   1
client9-20   1
client5-22   1
client25-331   1
client14-332   1
client17-333   1
client5-334   1
client24-335   1
client25-336   1
client22-337   1
client5-338   1
client9-339   1
client29-340   1
client21-341   1
/

Parameter
  ESV(i) /
nanny1   1
nanny2   0
nanny3   1
nanny4   1
nanny5   1
nanny6   1
nanny7   1
nanny8   1
nanny9   1
asdasd   0
/

Parameter
  ESG(j) /
client5-7   1
client9-20   0
client5-22   0
client25-331   0
client14-332   0
client17-333   0
client5-334   0
client24-335   0
client25-336   0
client22-337   0
client5-338   0
client9-339   0
client29-340   0
client21-341   0
/

Parameter
  bES(j) /
client5-7   0
client9-20   1
client5-22   1
client25-331   1
client14-332   1
client17-333   1
client5-334   1
client24-335   1
client25-336   1
client22-337   1
client5-338   1
client9-339   1
client29-340   1
client21-341   1
/

Parameter
  PSV(i) /
nanny1   1
nanny2   1
nanny3   0
nanny4   1
nanny5   1
nanny6   1
nanny7   0
nanny8   1
nanny9   1
asdasd   0
/

Parameter
  PSG(j) /
client5-7   0
client9-20   0
client5-22   0
client25-331   0
client14-332   0
client17-333   0
client5-334   0
client24-335   0
client25-336   0
client22-337   0
client5-338   0
client9-339   0
client29-340   0
client21-341   0
/

Parameter
  bPS(j) /
client5-7   1
client9-20   1
client5-22   1
client25-331   1
client14-332   1
client17-333   1
client5-334   1
client24-335   1
client25-336   1
client22-337   1
client5-338   1
client9-339   1
client29-340   1
client21-341   1
/

Parameter
  SSV(i) /
nanny1   1
nanny2   1
nanny3   0
nanny4   1
nanny5   1
nanny6   1
nanny7   0
nanny8   1
nanny9   1
asdasd   0
/

Parameter
  SSG(j) /
client5-7   0
client9-20   0
client5-22   1
client25-331   0
client14-332   0
client17-333   0
client5-334   0
client24-335   0
client25-336   0
client22-337   0
client5-338   0
client9-339   0
client29-340   0
client21-341   0
/

Parameter
  bSS(j) /
client5-7   1
client9-20   1
client5-22   0
client25-331   1
client14-332   1
client17-333   1
client5-334   1
client24-335   1
client25-336   1
client22-337   1
client5-338   1
client9-339   1
client29-340   1
client21-341   1
/

Parameter
  P(i) /
nanny1   11.0
nanny2   20.0
nanny3   21.0
nanny4   19.0
nanny5   14.0
nanny6   8.0
nanny7   24.0
nanny8   19.0
nanny9   12.0
asdasd   50.0
/

Parameter
  ZB(j) /
client5-7   37.0
client9-20   30.0
client5-22   29.0
client25-331   50.0
client14-332   48.0
client17-333   54.0
client5-334   51.0
client24-335   58.0
client25-336   47.0
client22-337   46.0
client5-338   58.0
client9-339   47.0
client29-340   48.0
client21-341   52.0
/

Parameter
  qV(i) /
nanny1   2.0
nanny2   2.0
nanny3   3.0
nanny4   3.0
nanny5   2.0
nanny6   2.0
nanny7   1.0
nanny8   5.0
nanny9   1.0
asdasd   0
/

Parameter
  qG(j) /
client5-7   3.0
client9-20   3.0
client5-22   3.0
client25-331   5.0
client14-332   3.0
client17-333   4.0
client5-334   2.0
client24-335   1.0
client25-336   2.0
client22-337   2.0
client5-338   3.0
client9-339   3.0
client29-340   4.0
client21-341   1.0
/

Parameter
  ageNanny(i) /
nanny1   28
nanny2   21
nanny3   36
nanny4   32
nanny5   32
nanny6   30
nanny7   27
nanny8   37
nanny9   23
asdasd   0
/

Parameter
  ageNmax(j) /
client5-7   61
client9-20   52
client5-22   58
client25-331   62
client14-332   65
client17-333   55
client5-334   61
client24-335   45
client25-336   45
client22-337   46
client5-338   46
client9-339   44
client29-340   47
client21-341   55
/

Parameter
  ageNmin(j) /
client5-7   18
client9-20   19
client5-22   19
client25-331   19
client14-332   19
client17-333   20
client5-334   20
client24-335   20
client25-336   20
client22-337   18
client5-338   19
client9-339   20
client29-340   19
client21-341   19
/

