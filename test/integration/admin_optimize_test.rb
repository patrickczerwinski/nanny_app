require 'test_helper'

# Einzeln testen mit: rake test TEST=test/integration/others/admin_optimize_test.rb

class AdminOptimizeTest < ActionDispatch::IntegrationTest

  setup do
    @admin = users(:admintest)

  end

  test "links on adminoptimize page" do
    log_in_as(@admin)
    get admin_optimize_path
    assert_template 'static_pages/admin_optimize'

    assert_select "a[href=?]", optimize_path
    assert_select "a[href=?]", read_solution_path
    assert_select "a[href=?]", gamstest_path

  end



end