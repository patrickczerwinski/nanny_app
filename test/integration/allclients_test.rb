require 'test_helper'

# Einzeln testen mit: rake test TEST=test/integration/allclients_test.rb


class AllClientsTest < ActionDispatch::IntegrationTest

  def setup
    @admin     = users(:admintest)
    @non_admin = users(:clienttest)
  end

  test "allclients as admin including delete link and requestcount" do
    log_in_as(@admin)
    get allclients_path
    assert_template 'users/indexclient'
    nannieslist = User.where(role:"client")
    nannieslist.each do |user|
      assert_select 'a[href=?]', user_path(user), text: user.name
      assert_select 'a[href=?]', user_path(user), text: 'Löschen'
      assert_select 'a#requestscount',  text: "| Gesamt Aufträge: " + Request.where(name: user.name).count.to_s


    end
    assert_difference 'User.count', -1 do
      delete user_path(@non_admin)
    end
  end


end