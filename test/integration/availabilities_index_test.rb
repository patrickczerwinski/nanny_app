require 'test_helper'

# Einzeln testen mit: rake test TEST=test/integration/availabilities/availabilities_index_test.rb


class AvailabilitiesIndexTest < ActionDispatch::IntegrationTest

  def setup
    @admin     = users(:admintest)
    @nannyaccount = users(:nanny1)
    @nanny = nannies(:nanny1)

  end


  test "index as nanny" do
    log_in_as(@nannyaccount)
    get availabilities_path
    assert_template 'availabilities/index'

    #Perma-Links zum Ausfüllen/Löschen von mehreren Verfügbarkeiten
    assert_select 'a[href=?]', addtimes_path(nannyname: @nanny.name, time: "morning"), text: 'Morgens'
    assert_select 'a[href=?]', addtimes_path(nannyname: @nanny.name, time: "noon"), text: 'Mittags'
    assert_select 'a[href=?]', addtimes_path(nannyname: @nanny.name, time: "afternoon"), text: 'Nachmittags'
    assert_select 'a[href=?]', addtimes_path(nannyname: @nanny.name, time: "evening"), text: 'Abends'

    assert_select 'a[href=?]', addtimes_path(nannyname: @nanny.name, time: "morning", m: "delete"), text: 'Löschen'
    assert_select 'a[href=?]', addtimes_path(nannyname: @nanny.name, time: "noon", m: "delete"), text: 'Löschen'
    assert_select 'a[href=?]', addtimes_path(nannyname: @nanny.name, time: "afternoon", m: "delete"), text: 'Löschen'
    assert_select 'a[href=?]', addtimes_path(nannyname: @nanny.name, time: "evening", m: "delete"), text: 'Löschen'

    allavailabilities = Availability.where(nannyname: @nanny.name)
    allavailabilities.each do |availability|
      assert_select 'div#nannyname',  text: availability.nannyname
      assert_select 'div#date',  text: availability.date
      assert_not_empty 'div#weekday'
      assert_select 'a[href=?]', edit_availability_path(availability), text: 'Bearbeiten'

      # Prüfen, ob die richtigen Farbfelder gewählt werden.
      # Hier konkret für den Fall der Availability "fornanny1"
      if availability.morning != 1
        else
        assert_select 'div#freeava'
      end
      if availability.afternoon != 0
        else
        assert_select 'div#busyava'
      end

      if availability.evening != 2
        else
        assert_select 'a#assignedava'
      end

    end

  end


  test "index as admin" do
    log_in_as(@admin)
    get availabilities_path(nannyname: @nanny.name)
    assert_template 'availabilities/index'

    #Perma-Links zum Ausfüllen/Löschen von mehreren Verfügbarkeiten
    assert_select 'a[href=?]', addtimes_path(nannyname: @nanny.name, time: "morning"), text: 'Morgens'
    assert_select 'a[href=?]', addtimes_path(nannyname: @nanny.name, time: "noon"), text: 'Mittags'
    assert_select 'a[href=?]', addtimes_path(nannyname: @nanny.name, time: "afternoon"), text: 'Nachmittags'
    assert_select 'a[href=?]', addtimes_path(nannyname: @nanny.name, time: "evening"), text: 'Abends'

    assert_select 'a[href=?]', addtimes_path(nannyname: @nanny.name, time: "morning", m: "delete"), text: 'Löschen'
    assert_select 'a[href=?]', addtimes_path(nannyname: @nanny.name, time: "noon", m: "delete"), text: 'Löschen'
    assert_select 'a[href=?]', addtimes_path(nannyname: @nanny.name, time: "afternoon", m: "delete"), text: 'Löschen'
    assert_select 'a[href=?]', addtimes_path(nannyname: @nanny.name, time: "evening", m: "delete"), text: 'Löschen'

    allavailabilities = Availability.where(nannyname: @nanny.name)
    allavailabilities.each do |availability|
      assert_select 'div#nannyname',  text: availability.nannyname
      assert_select 'div#date',  text: availability.date
      assert_not_empty 'div#weekday'
      assert_select 'a[href=?]', edit_availability_path(availability), text: 'Bearbeiten'

      # Prüfen, ob die richtigen Farbfelder gewählt werden.
      # Hier konkret für den Fall der Availability "fornanny1"
      if availability.morning != 1
        else
        assert_select 'div#freeava'
      end
      if availability.afternoon != 0
        else
        assert_select 'div#busyava'
      end

      if availability.evening != 2
        else
        assert_select 'a#assignedava'
      end

    end

  end


end