require 'test_helper'

# Einzeln testen mit: rake test TEST=test/integration/nannies/nannies_edit_test.rb


class NanniesEditTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:nanny1)
    @nanny  = nannies(:nanny1)
  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_nanny_path(@nanny)
    assert_template 'nannies/edit'
    patch nanny_path(@nanny), nanny: { price:  nil }
    assert_template 'nannies/edit'
  end

  test "successful edit" do
    log_in_as(@user)
    get edit_nanny_path(@nanny)

    price = 10.0
    patch nanny_path(@nanny), nanny: { price: price }
    assert_not flash.empty?
    assert_redirected_to edit_nanny_path(@nanny)
    @nanny.reload
    assert_equal price,  @nanny.price

  end






end
