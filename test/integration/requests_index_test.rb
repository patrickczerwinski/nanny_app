require 'test_helper'

# Einzeln testen mit: rake test TEST=test/integration/requests/requests_index_test.rb


class RequestsIndexTest < ActionDispatch::IntegrationTest

  def setup
    @admin = users(:admintest)
    @client = users(:client2)
    @nanny = users(:nanny1)
  end

  test "index as admin" do
    log_in_as(@admin)
    get requests_path(whichview: "all")
    assert_template 'requests/index'

    assert_select 'a[href=?]', requestsdatatable_path, text: 'Datatables-View öffnen'
    assert_select 'a[href=?]', requests_path(whichview: "onlyold"), text: 'Nur vergangene Aufträge'
    assert_select 'a[href=?]', requests_path(whichview: "lastweek"), text: 'Letzte Woche'
    assert_select 'a[href=?]', requests_path(whichview: "onlynew"), text: 'Nur neue Aufträge'
    assert_select 'a[href=?]', requests_path(whichview: "all"), text: 'Alle Aufträge'

    requestslist = Request.all
    requestslist.each do |request|
      assert_select 'div#requestname', text: request.name
      assert_not_empty 'div#weekday'

      # Prüft welche Links angezeigt werden je nach Status
      if request.status == 0 or request.status == 2 or request.status == 3
        assert_select 'div#assignednanny', text: ""
        assert_select 'a[href=?]', request_path(request), text: 'Anzeigen'
        assert_select 'a[href=?]', edit_request_path(request), text: 'Bearbeiten'
        assert_select 'a[href=?]', request_path(request), text: 'Löschen'
      elsif request.status == 1
        assert_select 'div#assignednanny', text: request.assigned_nanny
        assert_select 'a[href=?]', request_path(request), text: 'Anzeigen'
      elsif request.status == 1 and request.date.to_date.past? == true and request.ratingdone == false
        assert_select 'div#assignednanny', text: request.assigned_nanny
        assert_select 'a[href=?]', request_path(request), text: 'Anzeigen'
        assert_select 'a[href=?]', request_path(request), text: 'Nanny bewerten'
      elsif request.status == 1 and request.date.to_date.past? == true and request.ratingdone == true
        assert_select 'div#assignednanny', text: request.assigned_nanny
        assert_select 'a[href=?]', request_path(request), text: 'Anzeigen'

      end
    end

  end

  test "index as client" do
    log_in_as(@client)
    get requests_path(whichview: "all")
    assert_template 'requests/index'

    assert_select 'a[href=?]', requestsdatatable_path, text: 'Datatables-View öffnen'
    assert_select 'a[href=?]', requests_path(whichview: "onlyold"), text: 'Nur vergangene Aufträge'
    assert_select 'a[href=?]', requests_path(whichview: "lastweek"), text: 'Letzte Woche'
    assert_select 'a[href=?]', requests_path(whichview: "onlynew"), text: 'Nur neue Aufträge'
    assert_select 'a[href=?]', requests_path(whichview: "all"), text: 'Alle Aufträge'

    requestslist = Request.where(name: @client.name)
    requestslist.each do |request|
      assert_select 'div#requestname', text: request.name
      assert_not_empty 'div#weekday'
      assert_not_empty 'div#time'

      # Prüft welche Links angezeigt werden je nach Status
      if request.status == 0 or request.status == 2 or request.status == 3
        assert_select 'div#assignednanny', text: ""
        assert_select 'a[href=?]', request_path(request), text: 'Anzeigen'
        assert_select 'a[href=?]', edit_request_path(request), text: 'Bearbeiten'
        assert_select 'a[href=?]', request_path(request), text: 'Löschen'
      elsif request.status == 1
        assert_select 'div#assignednanny', text: request.assigned_nanny
        assert_select 'a[href=?]', request_path(request), text: 'Anzeigen'
      elsif request.status == 1 and request.date.to_date.past? == true and request.ratingdone == false
        assert_select 'div#assignednanny', text: request.assigned_nanny
        assert_select 'a[href=?]', request_path(request), text: 'Anzeigen'
        assert_select 'a[href=?]', request_path(request), text: 'Nanny bewerten'
      elsif request.status == 1 and request.date.to_date.past? == true and request.ratingdone == true
        assert_select 'div#assignednanny', text: request.assigned_nanny
        assert_select 'a[href=?]', request_path(request), text: 'Anzeigen'

      end
    end

  end


  test "index as nanny" do
    log_in_as(@nanny)
    get requests_path(whichview: "all")
    assert_template 'requests/index'

    assert_select 'a[href=?]', requestsdatatable_path, text: 'Datatables-View öffnen'
    assert_select 'a[href=?]', requests_path(whichview: "onlyold"), text: 'Nur vergangene Aufträge'
    assert_select 'a[href=?]', requests_path(whichview: "lastweek"), text: 'Letzte Woche'
    assert_select 'a[href=?]', requests_path(whichview: "onlynew"), text: 'Nur neue Aufträge'
    assert_select 'a[href=?]', requests_path(whichview: "all"), text: 'Alle Aufträge'

    requestslist = Request.where(assigned_nanny: @nanny.name)
    requestslist.each do |request|
      assert_select 'div#requestname', text: request.name
      assert_not_empty 'div#weekday'
      assert_not_empty 'div#time'
      assert_select 'a[href=?]', request_path(request), text: 'Anzeigen'


    end

  end


end