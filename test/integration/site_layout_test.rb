require 'test_helper'

# Einzeln testen mit: rake test TEST=test/integration/site_layout_test.rb

class SiteLayoutTest < ActionDispatch::IntegrationTest

  setup do
    @admin = users(:admintest)
    @client = users(:clienttest)
    @nanny = users(:nanny1)
  end

  test "layout links when logged out" do
    get root_path
    assert_template 'static_pages/home'
    #Header
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", login_path

    #Footer
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path

    #Body
    assert_select "a[href=?]", newnanny_path, count: 2
    assert_select "a[href=?]", signup_path, count: 2

  end

  test "layout links when admin" do
    log_in_as(@admin)
    get admin_controll_path
    assert_template 'static_pages/admin_controll'
    #Header
    assert_select "a[href=?]", root_path
    assert_select "a[href=?]", simulator_path
    assert_select "a[href=?]", admin_controll_path
    assert_select "a[href=?]", admin_optimize_path
    assert_select "a[href=?]", nannies_path
    assert_select "a[href=?]", new_request_path
    assert_select "a[href=?]", users_path
    assert_select "a[href=?]", allclients_path
    assert_select "a[href=?]", edit_user_path(@admin)
    assert_select "a[href=?]", user_path(@admin)


    assert_select "a[href=?]", logout_path

    #Footer
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
  end

  test "layout links when client" do
    log_in_as(@client)
    get requests_path
    assert_template 'requests/index'
    #Header
    assert_select "a[href=?]", root_path
    assert_select "a[href=?]", new_request_path
    assert_select "a[href=?]", requests_path
    assert_select "a[href=?]", edit_user_path(@client)
    assert_select "a[href=?]", user_path(@client)
    assert_select "a[href=?]", logout_path

    #Footer
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
  end

  test "layout links when nanny" do
    log_in_as(@nanny)
    get requests_path
    assert_template 'requests/index'
    #Header
    assert_select "a[href=?]", root_path
    assert_select "a[href=?]", availabilities_path
    assert_select "a[href=?]", requests_path
    assert_select "a[href=?]", edit_user_path(@nanny)
    assert_select "a[href=?]", user_path(@nanny)
    assert_select "a[href=?]", logout_path

    #Footer
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
  end

end