require 'test_helper'

# Einzeln testen mit: rake test TEST=test/integration/nannies_index_test.rb


class NanniesIndexTest < ActionDispatch::IntegrationTest

  def setup
    @admin     = users(:admintest)
    @non_admin = users(:nanny1)
  end

  test "index as admin including availabilties, show, edit and delete links" do
    log_in_as(@admin)
    get nannies_path
    assert_template 'nannies/index'
    allnannies = Nanny.all
    allnannies.each do |user|
      assert_select 'div#nannyname',  text: user.name
      assert_select 'div#rating',  text: user.rating.to_s
      assert_select 'a[href=?]', availabilities_path(nannyname: user.name), text: 'Verfügbarkeiten'
      assert_select 'a[href=?]', nanny_path(user), text: 'Anzeigen'
      assert_select 'a[href=?]', edit_nanny_path(user), text: 'Bearbeiten'
      assert_select 'a[href=?]', nanny_path(user), text: 'Löschen'

    end
    assert_difference 'Nanny.count', -1 do
      delete nanny_path(@non_admin)
    end
  end


end