require 'test_helper'

# Einzeln testen mit: rake test TEST=test/controllers/nannies_controller_test.rb


class NanniesControllerTest < ActionController::TestCase
  setup do
    @admin = users(:admintest)
    @client = users(:clienttest)
    @nannyaccount = users(:nanny3)
    @oldnannyaccount = users(:nanny1)
    @nanny = nannies(:nanny1)

  end

  # Die Nanny Übersicht soll nur der Admin sehen können

  test "should get index if admin" do
    log_in_as(@admin)
    get :index
    assert_response :success
  end

  test "should not get index if nanny or client or logged out" do
    get :index
    assert_redirected_to login_path

    log_in_as(@nannyaccount)
    get :index
    assert_redirected_to noaccess_path

    log_in_as(@client)
    get :index
    assert_redirected_to noaccess_path
  end


  # Derzeit soll nur ein User selbst seinen Nanny-Account erstellen können
  test "should get new if nanny" do
    log_in_as(@nannyaccount)
    get :new
    assert_response :success
  end

  test "should redirect to edit if old nanny opens new" do
    log_in_as(@oldnannyaccount)
    get :new
    assert_redirected_to nanniesedit_path
  end


  test "should not get new if admin or client or logged out" do
    get :new
    assert_redirected_to login_path

    log_in_as(@admin)
    get :new
    assert_redirected_to noaccess_path

    log_in_as(@client)
    get :new
    assert_redirected_to noaccess_path
  end


  test "should create nanny if new nanny and redirect to availabilities" do
    log_in_as(@nannyaccount)
    assert_difference('Nanny.count') do
      post :create, nanny: { age: @nanny.age, driverlicense: @nanny.driverlicense,
                             education: @nanny.education, language_english: @nanny.language_english,
                             language_german: @nanny.language_german, language_polish: @nanny.language_polish,
                             language_spanish: @nanny.language_spanish, name: @nanny.name, price: @nanny.price,
                             rating: @nanny.rating, sex: @nanny.sex }
    end

    assert_redirected_to availabilities_path
  end


  # Nannys sollen von allen eingeloggten Benutzern angesehen werden können
  test "should show nanny as admin" do
    log_in_as(@admin)
    get :show, id: @nanny
    assert_response :success
  end

  test "should show nanny as client" do
    log_in_as(@client)
    get :show, id: @nanny
    assert_response :success
  end

  test "should show nanny as nanny" do
    log_in_as(@oldnannyaccount)
    get :show, id: @nanny
    assert_response :success
  end

  test "should not show nanny if logged out" do
    get :show, id: @nanny
    assert_redirected_to login_path

  end


  # Nur Admins und die Nanny selbst sollen ihre Nanny-Daten bearbeiten können

  test "should get edit if admin" do
    log_in_as(@admin)
    get :edit, id: @nanny
    assert_response :success
  end

  test "should get edit if nanny" do
    log_in_as(@oldnannyaccount)
    get :edit, id: @nanny
    assert_response :success
  end

  test "should be able to update as nanny" do
    log_in_as(@oldnannyaccount)
    patch :update, id: @nanny, nanny: { age: @nanny.age, driverlicense: @nanny.driverlicense, education: @nanny.education, language_english: @nanny.language_english, language_german: @nanny.language_german, language_polish: @nanny.language_polish, language_spanish: @nanny.language_spanish, name: @nanny.name, price: @nanny.price, rating: @nanny.rating, sex: @nanny.sex }
    assert_redirected_to edit_nanny_path(@nanny)
  end

  test "should be able to update as admin" do
    log_in_as(@admin)
    patch :update, id: @nanny, nanny: { age: @nanny.age, driverlicense: @nanny.driverlicense, education: @nanny.education, language_english: @nanny.language_english, language_german: @nanny.language_german, language_polish: @nanny.language_polish, language_spanish: @nanny.language_spanish, name: @nanny.name, price: @nanny.price, rating: @nanny.rating, sex: @nanny.sex }
    assert_redirected_to edit_nanny_path(@nanny)
  end



  test "should be able to destroy nanny if nanny" do
    log_in_as(@oldnannyaccount)
    assert_difference('Nanny.count', -1) do
      delete :destroy, id: @nanny
    end
    assert_redirected_to root_path
  end

  test "should be able to destroy nanny if admin" do
    log_in_as(@admin)
    assert_difference('Nanny.count', -1) do
      delete :destroy, id: @nanny
    end
    assert_redirected_to root_path
  end

end
