require 'test_helper'


# Einzeln testen mit: rake test TEST=test/controllers/availabilities_controller_test.rb


class AvailabilitiesControllerTest < ActionController::TestCase
  setup do
    @availability = availabilities(:fornanny1)
    @admin = users(:admintest)
    @client = users(:clienttest)
    @nanny = users(:nanny1)

    @request.env['HTTP_REFERER'] = 'http://test.com/sessions/new'

  end

  # Test Verfügbarkeiten Übersicht öffnen können, wenn als Nanny oder Admin eingeloggt

  test "should get index if nanny" do
    log_in_as(@nanny)
    get :index
    assert_response :success
    assert_not_nil assigns(:availabilities)
  end

  test "should get index if admin and params (with correct nanny) are given" do
    log_in_as(@admin)
    get :index, nannyname: "nanny1"
    assert_response :success
    assert_not_nil assigns(:availabilities)
  end

  test "should get nonanny if admin and params (with wrong nanny) are given" do
    log_in_as(@admin)
    get :index, nannyname: "nannynotgiven"
    assert_redirected_to nannies_path
    assert_equal flash[:warning], "Diese Nanny ist nicht vorhanden."
  end

  test "should get choosenanny if admin and empty params" do
    log_in_as(@admin)
    get :index
    assert_redirected_to nannies_path
    assert_equal flash[:warning], "Bitte Nanny auswählen."
  end


  test "should not get index if client" do
    log_in_as(@client)
    get :index
    assert_redirected_to noaccess_path
  end

  test "should not get index if not logged in" do
    get :index
    assert_redirected_to login_path
  end


  # Neue Verfügbarkeiten können nicht erstellt werden, da diese immer automatisch erzeugt und gelöscht werden.

  test "should never get new" do
    get :new
    assert_redirected_to login_path

    log_in_as(@admin)
    get :new
    assert_redirected_to noaccess_path

    log_in_as(@nanny)
    get :new
    assert_redirected_to noaccess_path

    log_in_as(@client)
    get :new
    assert_redirected_to noaccess_path

  end

  # Testen ob Admin und Nanny Verfügbarkeiten erzeugen können (damit die automatisierte Erzeugung funktioniert)

  test "should be able to create availability if admin" do
    log_in_as(@admin)
    assert_difference('Availability.count') do
      post :create, availability: { afternoon: @availability.afternoon, date: @availability.date,
                                    evening: @availability.evening, morning: @availability.morning,
                                    nannyname: @availability.nannyname, noon: @availability.noon }
    end
  end

  test "should be able to create availability if nanny" do
    log_in_as(@nanny)
    assert_difference('Availability.count') do
      post :create, availability: { afternoon: @availability.afternoon, date: @availability.date,
                                    evening: @availability.evening, morning: @availability.morning,
                                    nannyname: @availability.nannyname, noon: @availability.noon }
    end
  end

  # Anzeige von Verfügbarkeiten prüfen

  test "should never show availability" do
    get :show, id: @availability
    assert_redirected_to login_path

    log_in_as(@admin)
    get :show, id: @availability
    assert_redirected_to noaccess_path

    log_in_as(@nanny)
    get :show, id: @availability
    assert_redirected_to noaccess_path

    log_in_as(@client)
    get :show, id: @availability
    assert_redirected_to noaccess_path

  end


  # Bearbeitung von Verfügbarkeiten prüfen

  test "should get edit if nanny" do
    log_in_as(@nanny)
    get :edit, id: @availability
    assert_response :success
  end

  test "should get edit if admin" do
    log_in_as(@admin)
    get :edit, id: @availability
    assert_response :success
  end

  test "should not get edit if client or not logged in" do
    get :edit, id: @availability
    assert_redirected_to login_path

    log_in_as(@client)
    get :edit, id: @availability
    assert_redirected_to noaccess_path
  end


  test "should update availability if nanny" do
    log_in_as(@nanny)
    patch :update, id: @availability, availability: { afternoon: @availability.afternoon,
                                                      date: @availability.date, evening: @availability.evening,
                                                      morning: @availability.morning,
                                                      nannyname: @availability.nannyname,
                                                      noon: @availability.noon }
    assert_redirected_to edit_availability_path(@availability)
  end

  test "should update availability if admin" do
    log_in_as(@admin)
    patch :update, id: @availability, availability: { afternoon: @availability.afternoon,
                                                      date: @availability.date, evening: @availability.evening,
                                                      morning: @availability.morning,
                                                      nannyname: @availability.nannyname,
                                                      noon: @availability.noon }
    assert_redirected_to edit_availability_path(@availability)
  end



  # Nannys und Admins müssen die Berechtigung haben, Verfügbarkeiten zu löschen (passiert automatisiert)
  test "should be able to destroy availability if admin" do
    log_in_as(@admin)
    assert_difference('Availability.count', -1) do
      delete :destroy, id: @availability
    end
  end

  test "should be able to destroy availability if nanny" do
    log_in_as(@nanny)
    assert_difference('Availability.count', -1) do
      delete :destroy, id: @availability
    end
  end

end
