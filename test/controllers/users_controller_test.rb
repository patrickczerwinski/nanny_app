require 'test_helper'

# Einzeln testen mit: rake test TEST=test/controllers/users_controller_test.rb


class UsersControllerTest < ActionController::TestCase
  def setup
    @user = users(:admintest)
    @other_user = users(:clienttest)
  end

  test "should redirect index when not logged in" do
    get :index
    assert_redirected_to login_url
  end

  # Registrierungen nur ermöglichen, wenn man gerade nicht eingeloggt ist.

  test "should get new if logged out" do
    get :new
    assert_response :success
  end

  test "should not get new if logged in" do
    log_in_as(@user)
    get :new
    assert_redirected_to @user
  end


  test "should get newnanny" do
    get :newnanny
    assert_response :success
  end

  test "should not get newnanny if logged in" do
    log_in_as(@user)
    get :newnanny
    assert_redirected_to @user
  end

  test "should redirect edit when not logged in" do
    get :edit, id: @user
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch :update, id: @user, user: { name: @user.name, email: @user.email }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect edit when logged in as wrong user" do
    log_in_as(@other_user)
    get :edit, id: @user
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect update when logged in as wrong user" do
    log_in_as(@other_user)
    patch :update, id: @user, user: { name: @user.name, email: @user.email }
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do
      delete :destroy, id: @user
    end
    assert_redirected_to login_url
  end

  test "should redirect to root path when destroy own account" do
    log_in_as(@other_user)
    assert_difference('User.count', -1) do
      delete :destroy, id: @other_user
    end
    assert_redirected_to root_path
  end

  test "should redirect to users when destroy other account as admin" do
    log_in_as(@user)
    assert_difference('User.count', -1) do
      delete :destroy, id: @other_user
    end
    assert_redirected_to users_path
  end

end
