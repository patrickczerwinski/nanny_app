require 'test_helper'

# Einzeln testen mit: rake test TEST=test/controllers/requests_controller_test.rb


class RequestsControllerTest < ActionController::TestCase
  setup do

    @admin = users(:admintest)
    @client = users(:client1)
    @other_client = users(:client2)
    @nanny = users(:nanny1)
    @other_nanny = users(:nanny2)
    @myrequestzero = requests(:client1requeststatus0)
    @myrequestone = requests(:client2requeststatus1)
    @myrequesttwo = requests(:client2requeststatus2)
    @myrequestthree= requests(:client2requeststatus3)

  end

  # Auf Aufträge sollen nur eingeloggte Nutzer zugreifen können

  test "should not get index" do
    get :index
    assert_redirected_to login_path
  end

  test "should get index if admin" do
    log_in_as(@admin)
    get :index
    assert_response :success
  end

  test "should get index if client" do
    log_in_as(@client)
    get :index
    assert_response :success
  end

  test "should get index if nanny" do
    log_in_as(@nanny)
    get :index
    assert_response :success

  end


  # Neue Aufträge können von Kunden und dem Admin erstellt werden
  test "should get new if admin" do
    log_in_as(@admin)
    get :new
    assert_response :success
  end

  test "should get new if client" do
    log_in_as(@client)
    get :new
    assert_response :success
  end

  test "should not get new if logged out" do
    get :new
    assert_redirected_to login_path

    log_in_as(@nanny)
    get :new
    assert_redirected_to noaccess_path
  end

  test "should not get new if nanny" do
    log_in_as(@nanny)
    get :new
    assert_redirected_to noaccess_path
  end

  test "should be able create request if admin" do
    log_in_as(@admin)
    assert_difference('Request.count') do
      post :create, request: {
                               date: @myrequestzero.date, driverlicense: @myrequestzero.driverlicense, 
                               education: @myrequestzero.education, language_english: @myrequestzero.language_english, 
                               language_german: @myrequestzero.language_german, language_polish: @myrequestzero.language_polish, 
                               language_spanish: @myrequestzero.language_spanish, maxage: @myrequestzero.maxage, 
                               minage: @myrequestzero.minage, name: @myrequestzero.name, rating: @myrequestzero.rating,
                               sex: @myrequestzero.sex, time: @myrequestzero.time, willingness_to_pay: @myrequestzero.willingness_to_pay }
    end
    assert_redirected_to request_path(assigns(:request))
  end

  test "should be able create request if client" do
    log_in_as(@client)
    assert_difference('Request.count') do
      post :create, request: {
          date: @myrequestzero.date, driverlicense: @myrequestzero.driverlicense,
          education: @myrequestzero.education, language_english: @myrequestzero.language_english,
          language_german: @myrequestzero.language_german, language_polish: @myrequestzero.language_polish,
          language_spanish: @myrequestzero.language_spanish, maxage: @myrequestzero.maxage,
          minage: @myrequestzero.minage, name: @myrequestzero.name, rating: @myrequestzero.rating,
          sex: @myrequestzero.sex, time: @myrequestzero.time, willingness_to_pay: @myrequestzero.willingness_to_pay }
    end
    assert_redirected_to request_path(assigns(:request))
  end


  # Jeder kann nur seine eigenen Aufträge sehen, außer der Admin

  test "should show request if admin" do
    log_in_as(@admin)
    get :show, id: @myrequestzero
    assert_response :success
  end

  test "should show own request as client" do
    log_in_as(@client)
    get :show, id: @myrequestzero
    assert_response :success
  end

  test "should show own request as nanny" do
    log_in_as(@nanny)
    get :show, id: @myrequestone
    assert_response :success
  end

  test "should not show other clients request as client" do
    log_in_as(@other_client)
    get :show, id: @myrequestzero
    assert_redirected_to noaccess_path
  end

  test "should not show others request as nanny" do
    log_in_as(@other_nanny)
    get :show, id: @myrequestzero
    assert_redirected_to noaccess_path
  end

  test "should not show request if logged out" do
    get :show, id: @myrequestzero
    assert_redirected_to login_path
  end


  # Nur Admin und Kunden können Aufträge bearbeiten 

  test "should not get edit if nanny" do
    log_in_as(@nanny)
    get :edit, id: @myrequestone
    assert_redirected_to noaccess_path
  end

  test "should not get edit if logged out" do
    get :edit, id: @myrequestone
    assert_redirected_to login_path
  end

  test "should get edit if admin" do
    log_in_as(@admin)
    get :edit, id: @myrequestzero
    assert_response :success
  end

  test "should be able to update request if admin" do
    log_in_as(@admin)
    patch :update, id: @myrequestzero, request: { age: @myrequestzero.age, assigned_nanny: @myrequestzero.assigned_nanny,
                                              date: @myrequestzero.date, driverlicense: @myrequestzero.driverlicense,
                                              education: @myrequestzero.education, language_english: @myrequestzero.language_english,
                                              language_german: @myrequestzero.language_german, language_polish: @myrequestzero.language_polish,
                                              language_spanish: @myrequestzero.language_spanish, maxage: @myrequestzero.maxage,
                                              minage: @myrequestzero.minage, name: @myrequestzero.name, rating: @myrequestzero.rating,
                                              sex: @myrequestzero.sex, time: @myrequestzero.time,
                                              willingness_to_pay: @myrequestzero.willingness_to_pay }
    assert_redirected_to request_path(assigns(:request))
  end

  # Und Kunden können nur den eigenen Auftrag bearbeiten

  test "should get own edit if client" do
    log_in_as(@client)
    get :edit, id: @myrequestzero
    assert_response :success
  end

  test "should not get other edit if client" do
    log_in_as(@client)
    get :edit, id: @myrequesttwo
    assert_redirected_to noaccess_path
  end

  # Nur Aufträge mit dem Status 0 und 2 dürfen bearbeiten werden

  test "should get edit if status 0" do
    log_in_as(@client)
    get :edit, id: @myrequestzero
    assert_response :success
  end

  test "should get edit if status 2" do
    log_in_as(@other_client)
    get :edit, id: @myrequesttwo
    assert_response :success
  end

  test "should not get edit if status 1" do
    log_in_as(@other_client)
    get :edit, id: @myrequestone
    assert_redirected_to noaccess_path
  end

  test "should not get edit if status 3" do
    log_in_as(@other_client)
    get :edit, id: @myrequestthree
    assert_redirected_to noaccess_path
  end


  # Nur der Admins und Kunden können Aufträge löschen
  # Kunden können nur ihre eigenene Aufträge löschen
  # Nur Aufträge mit dem Status 0 und 2 können gelöscht werden

  test "should destroy request if admin" do
    log_in_as(@admin)
    assert_difference('Request.count', -1) do
      delete :destroy, id: @myrequestzero
    end
    assert_redirected_to requests_path
  end

  test "should destroy request if client and own request" do
    log_in_as(@client)
    assert_difference('Request.count', -1) do
      delete :destroy, id: @myrequestzero
    end
    assert_redirected_to requests_path
  end

  test "should not destroy request if client and other request" do
    log_in_as(@client)
    assert_no_difference 'Request.count' do
      delete :destroy, id: @myrequestone
    end
    assert_redirected_to noaccess_path
  end

  test "should not destroy request if nanny" do
    log_in_as(@nanny)
    assert_no_difference 'Request.count' do
      delete :destroy, id: @myrequestone
    end
    assert_redirected_to noaccess_path
  end

  test "should not destroy request if logged out" do
    assert_no_difference 'Request.count' do
      delete :destroy, id: @myrequestone
    end
    assert_redirected_to login_path
  end

  # Nur Status 0 und 2
  test "should  destroy request if status 0" do
    log_in_as(@client)
    assert_difference('Request.count', -1) do
      delete :destroy, id: @myrequestzero
    end
    assert_redirected_to requests_path
  end

  test "should not destroy request if status 1" do
    log_in_as(@other_client)
    assert_no_difference 'Request.count' do
      delete :destroy, id: @myrequestone
    end
    assert_redirected_to noaccess_path
  end

  test "should destroy request if status 2" do
    log_in_as(@other_client)
    assert_difference('Request.count', -1) do
      delete :destroy, id: @myrequesttwo
    end
    assert_redirected_to requests_path
  end

  test "should not destroy request if status 3" do
    log_in_as(@other_client)
    assert_no_difference 'Request.count' do
      delete :destroy, id: @myrequestthree
    end
    assert_redirected_to noaccess_path
  end

end
