require 'test_helper'

# Einzeln testen mit: rake test TEST=test/controllers/static_pages_controller_test.rb


class StaticPagesControllerTest < ActionController::TestCase

  def setup
    @admin = users(:admintest)
    @client = users(:clienttest)
    @nanny = users(:nannytest)

  end

  # Tests für verschiedene Startseiten

  test "should get home if not logged in" do
    get :home
    assert_response :success
    assert_select "title", "Home | EN"
  end

  test "should get dashboard on home if admin" do
    log_in_as(@admin)
    get :home
    assert_redirected_to admin_controll_path
  end

  test "should get requests on home if client" do
    log_in_as(@client)
    get :home
    assert_redirected_to requests_path
  end

  test "should get requests on home if nanny" do
    log_in_as(@nanny)
    get :home
    assert_redirected_to requests_path
  end

  # Tests um zu prüfen, ob nur der Admin auf die Adminseiten kann (Dashboard, Optimieren, Simulator)

    # Dashboard

  test "should get dashboard if admin" do
    log_in_as(@admin)
    get :admin_controll
    assert_response :success
    assert_select "title", "Dashboard | EN"
  end

  test "should not get dashboard if nanny" do
    log_in_as(@nanny)
    get :admin_controll
    assert_redirected_to noaccess_path
  end

  test "should not get dashboard if client" do
    log_in_as(@client)
    get :admin_controll
    assert_redirected_to noaccess_path
  end

  test "should not get dashboard if not logged in" do
    get :admin_controll
    assert_redirected_to noaccess_path
  end

    # Optimieren

  test "should get optimze if admin" do
    log_in_as(@admin)
    get :admin_optimize
    assert_response :success
    assert_select "title", "Optimierung | EN"
  end

  test "should not get optimize if client" do
    log_in_as(@client)
    get :admin_optimize
    assert_redirected_to noaccess_path
  end

  test "should not get optimize if nanny" do
    log_in_as(@nanny)
    get :admin_optimize
    assert_redirected_to noaccess_path
  end

  test "should not get optimize if not logged in" do
    get :admin_optimize
    assert_redirected_to noaccess_path
  end

    # Simulator

  test "should get simulator if admin" do
    log_in_as(@admin)
    get :simulator
    assert_response :success
    assert_select "title", "Simulator | EN"
  end

  test "should not get simulator if client" do
    log_in_as(@client)
    get :simulator
    assert_redirected_to noaccess_path
  end

  test "should not get simulator if nanny" do
    log_in_as(@nanny)
    get :simulator
    assert_redirected_to noaccess_path
  end

  test "should not get simulator if not logged in" do
    get :simulator
    assert_redirected_to noaccess_path
  end


  # Sonstige Tests

  test "should get help" do
    get :help
    assert_response :success
    assert_select "title", "Anleitung | EN"
  end

  test "should get about" do
    get :about
    assert_response :success
    assert_select "title", "Informationen | EN"
  end

  test "should get contact" do
    get :contact
    assert_response :success
    assert_select "title", "Kontakt | EN"
  end

  test "should get noaccess" do
    get :noaccess
    assert_response :success
    assert_select "title", "Fehler | EN"
  end

end
