require 'test_helper'

# Einzeln testen mit: rake test TEST=test/controllers/options_controller_test.rb


class OptionsControllerTest < ActionController::TestCase
  setup do
    @option = options(:gams)
    @admin = users(:admintest)
  end


  test "should be able to create option if admin" do
    log_in_as(@admin)
    assert_difference('Option.count') do
      post :create, option: { optionname: @option.optionname, value: @option.value }
    end
    assert_redirected_to admin_optimize_path
  end

  test "should be able to update option if admin" do
    log_in_as(@admin)
    patch :update, id: @option, option: { optionname: @option.optionname, value: @option.value }
    assert_redirected_to admin_optimize_path
  end

  test "should be able to destroy option if admin" do
    log_in_as(@admin)
    assert_difference('Option.count', -1) do
      delete :destroy, id: @option
    end
    assert_redirected_to admin_optimize_path
  end
end
