require 'test_helper'

# Einzeln testen mit: rake test TEST=test/models/nanny_test.rb

class NannyTest < ActiveSupport::TestCase

  def setup
    @nanny = Nanny.new(name: "nanny1", sex: "w", education: true, driverlicense: true, language_german: true,
                      language_english: false, language_spanish: true, language_polish: true,
                       price: 15.0, rating: 3 , ratingscount: 36)

  end

  test "should be valid" do
    assert @nanny.valid?
  end

  test "price should be present" do
    @nanny.price = nil
    assert_not @nanny.valid?
  end

  test "price should not be negative" do
    @nanny.price = -1
    assert_not @nanny.valid?
  end

  test "price should not be above 500" do
    @nanny.price = 501
    assert_not @nanny.valid?
  end

  test "age should not be negative" do
    @nanny.age = -2
    assert_not @nanny.valid?
  end

  test "age should not be above 100" do
    @nanny.age = 101
    assert_not @nanny.valid?
  end


end
