require 'test_helper'

# Einzeln testen mit: rake test TEST=test/models/request_test.rb


class RequestTest < ActiveSupport::TestCase

    def setup
      @newrequest = Request.new( age: 22, assigned_nanny: nil,
                          date: "2017-03-01", driverlicense: false,
                          education: false, language_english: false,
                          language_german: false, language_polish: false,
                          language_spanish: true, maxage: 50,
                          minage: nil, name: "client1", rating: 5,
                          sex: "w", time: "morning", willingness_to_pay: 20.0 )

    end

    test "should be valid" do
      assert @newrequest.valid?
    end

    test "date should be present" do
      @newrequest.date = nil
      assert_not @newrequest.valid?
    end

    test "time should be present" do
      @newrequest.time = nil
      assert_not @newrequest.valid?
    end

    test "willingness_to_pay should not be negative" do
      @newrequest.willingness_to_pay = -1
      assert_not @newrequest.valid?
    end

    test "willingness_to_pay should not be above 500" do
      @newrequest.willingness_to_pay = 501
      assert_not @newrequest.valid?
    end

    test "minage should not be negative" do
      @newrequest.minage = -1
      assert_not @newrequest.valid?
    end

    test "minage should not be above 100" do
      @newrequest.minage = 101
      assert_not @newrequest.valid?
    end

    test "maxage should not be negative" do
      @newrequest.maxage = -1
      assert_not @newrequest.valid?
    end

    test "maxage should not be above 100" do
      @newrequest.maxage = 101
      assert_not @newrequest.valid?
    end

end
