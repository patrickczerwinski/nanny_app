require 'test_helper'

# Einzeln testen mit: rake test TEST=test/models/availability_test.rb

class AvailabilityTest < ActiveSupport::TestCase
  def setup
    @availability = Availability.new(nannyname: "nanny1", date: "2017-02-19", morning: 1, noon: 0,
                      afternoon: 1, evening: 2)

  end

  test "should be valid" do
    assert @availability.valid?
  end



end
