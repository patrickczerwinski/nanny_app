require 'test_helper'

# Einzeln testen mit: rake test TEST=test/models/option_test.rb

class OptionTest < ActiveSupport::TestCase

  def setup
    @option = Option.new(optionname: "gamspath", value: "somepath")

  end

  test "should be valid" do
    assert @option.valid?
  end

end
