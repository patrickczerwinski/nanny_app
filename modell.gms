$include include.inc

Binary Variables
x(i,j,t,s) Zuordnung der Nanny i zum Kunden j in Zeitraum s t;

Free Variables
ZFW Zielfunktionswert;




Equations
Zielfunktion
JederKundeEinerNanny
JedeNannyMaximaleinKunde
Deckung
Geschlecht
Fuehrerschein
Zeit
Deutsch
Englisch
Polnisch
Spanisch
Quali
Ausbildung
MaxAlterDerNanny
MinAlterDerNanny
;

Zielfunktion..
                 ZFW =e= sum((i,j,t,s), x(i,j,t,s) * r ) ;

JederKundeEinerNanny(i,t,s)..
                 sum(j, x(i,j,t,s)) =l= 1;

JedeNannyMaximaleinKunde(j,t,s)..
                 sum(i, x(i,j,t,s)) =l= 1;


Deckung(i,j,t,s)..
                 x(i,j,t,s)*(ZB(j)-P(i)) =g= 0   ;

Geschlecht(i,j,t,s)..
                 (GMV(i)*GMG(j)) + (GWV(i)*GWG(j)) + bG(j) =g= x(i,j,t,s);

Fuehrerschein(i,j,t,s)..
                 FV(i) * FG(j) + bF(j) =g= x(i,j,t,s) ;

Zeit(i,j,t,s)..
                 tV(i,t,s)*tG(j,t,s) =g= x(i,j,t,s);

Deutsch(i,j,t,s)..
                 DSG(j) * DSV(i) + bDS(j) =g= x(i,j,t,s) ;

Englisch(i,j,t,s)..
                 ESG(j) * ESV(i) + bES(j) =g= x(i,j,t,s) ;

Polnisch(i,j,t,s)..
                 PSG(j) * PSV(i) + bPS(j) =g= x(i,j,t,s) ;

Spanisch(i,j,t,s)..
                 SSG(j) * SSV(i) + bSS(j) =g= x(i,j,t,s) ;

Quali(i,j,t,s)..
                 (qV(i) - qG(j))*x(i,j,t,s) =g= 0;

Ausbildung(i,j,t,s)..
                 aG(j)*aV(i) + bA(j) =g= x(i,j,t,s);

MaxAlterDerNanny(i,j,t,s)..
                 (ageNmax(j) - ageNanny(i))*x(i,j,t,s) =g= 0;

MinAlterDerNanny(i,j,t,s)..
                 (ageNanny(i) - ageNmin(j)*x(i,j,t,s)) =g= 0;




Model Nanny /all/;

Solve Nanny maximizing ZFW using MIP;



file outputfile1 / 'solution.txt'/;

put outputfile1;

put 'Zielfunktionswert:  ',ZFW.l /

loop(i,
     loop(j,
             loop(t,
                        loop(s,
                                     If (x.l(i,j,t,s) = 1.00,

             put i.tl:0, ' ; ' j.tl:0, ' ; ' t.tl:0, ' ; ' s.tl:0, ' ; ' x.l(i,j,t,s) /

                                        );
                        );

            );
     );
);

putclose outputfile1;




