# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170306173014) do

  create_table "availabilities", force: :cascade do |t|
    t.string   "nannyname"
    t.string   "date"
    t.integer  "morning"
    t.integer  "noon"
    t.integer  "afternoon"
    t.integer  "evening"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "nannies", force: :cascade do |t|
    t.string   "name"
    t.string   "sex"
    t.integer  "age"
    t.boolean  "education"
    t.boolean  "driverlicense"
    t.boolean  "language_german"
    t.boolean  "language_english"
    t.boolean  "language_spanish"
    t.boolean  "language_polish"
    t.float    "price"
    t.float    "rating"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "ratingscount"
    t.integer  "minage"
    t.integer  "maxage"
  end

  create_table "options", force: :cascade do |t|
    t.string   "optionname"
    t.string   "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "requests", force: :cascade do |t|
    t.string   "name"
    t.string   "sex"
    t.integer  "age"
    t.boolean  "education"
    t.boolean  "driverlicense"
    t.boolean  "language_german"
    t.boolean  "language_english"
    t.boolean  "language_spanish"
    t.boolean  "language_polish"
    t.float    "willingness_to_pay"
    t.float    "rating"
    t.string   "date"
    t.string   "time"
    t.integer  "minage"
    t.integer  "maxage"
    t.string   "assigned_nanny"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.boolean  "ratingdone"
    t.integer  "ratingvalue"
    t.integer  "status"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "password_digest"
    t.boolean  "admin",           default: false
    t.string   "city"
    t.string   "street"
    t.string   "role"
    t.string   "fullname"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
