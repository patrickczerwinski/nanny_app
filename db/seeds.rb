# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Zunächst für jede Rolle einen Testuser erstellen


User.create!(name: "testadmin",
             email: "admin@test.com",
             street: "Teststreet 5",
             city: "Testcity",
             password: "foobar",
             password_confirmation: "foobar",
             role: "admin")

User.create!(name: "nannytest",
             email: "nanny@test.com",
             street: "Teststreet 6",
             city: "Testcity",
             password: "foobar",
             password_confirmation: "foobar",
             role: "nanny")

User.create!(name: "clienttest",
             email: "client@test.com",
             street: "Teststreet 6",
             city: "Testcity",
             password: "foobar",
             password_confirmation: "foobar",
             role: "client")

# Optionszeile mit GAMS-Pfad erzeugen

Option.create(id: 1, optionname: "gamspath", value: "Hier neuen GAMS-Pfad einfügen..")


# 30 weitere zufällige Kunden hinzufügen
$i = 1

30.times do

  city = Faker::Address.city
  street = Faker::Address.street_address
  fullname = Faker::Name.first_name + " " + Faker::Name.last_name
  email = Faker::Internet.free_email
  name = "client" + $i.to_s

  $i +=1

  User.create!(name: name, email: email,
               password: "123456", password_confirmation: "123456",
               role: "client", city: city, street: street,
               fullname: fullname)

end

# 10 weitere zufällige Nannys hinzufügen

$i = 1

10.times do

# Zuerst den User der Nanny erstellen


city = Faker::Address.city
street = Faker::Address.street_address
fullname = Faker::Name.first_name + " " + Faker::Name.last_name
email = Faker::Internet.safe_email
name = "nanny" + $i.to_s

$i +=1

User.create!(name: name, email: email,
             password: "123456", password_confirmation: "123456",
             role: "nanny", city: city, street: street,
             fullname: fullname)


# Dann die Nanny-Daten erstellen

sex = ["w", "w", "m"].sample
education = [true, true, true, false].sample
driverlicense = [true, true, true, false].sample
languageG = [true, true, true, false].sample
languageE = [true, true, true, false].sample
languageS = [true, true, true, false].sample
languageP = [true, true, true, false].sample
price = rand(8..25)
rating =  rand(1..5).to_i
ratingscount =  rand(1..20)
age = rand(18..40).to_i

Nanny.create!(name: name, sex: sex, age: age, education: education, driverlicense: driverlicense,
              language_german: languageG, language_english: languageE, language_spanish: languageS, language_polish: languageP,
              price: price, rating: rating, ratingscount: ratingscount)


# Schließlich noch die Verfügbarkeiten der Nanny hinzufügen

7.times do |n|

  availdate = Date.today + n
  morny =  [0,1,1,1].sample
  noony =  [0,1,1,1].sample
  afternoony = [0,1,1,1].sample
  eveningy =  [0,1,1,1].sample

  Availability.create!(nannyname: name, date: availdate, morning: morny, noon: noony, afternoon: afternoony,
                       evening: eveningy)
end

end


# 30 Neue Anfragen erstellen

30.times do
  client = User.where(role: "client").sample

  sex = ["w", "w", "m"].sample
  education = [true, false, false, false].sample
  driverlicense = [true, false, false, false].sample
  languageG = [true, false, false, false].sample
  languageE = [true, false, false, false].sample
  languageS = [true, false, false, false].sample
  languageP = [true, false, false, false].sample
  willpay = rand(20..50)
  reqday  =  Date.today + rand(0..6)
  rating =  rand(1..3).to_i
  time =     ["morning", "noon", "afternoon", "evening"].sample
  minage = rand(18..20).to_i
  maxage = rand(38..65).to_i

  Request.create!(name: client.name, sex: sex, education: education, driverlicense: driverlicense,
                  language_german: languageG, language_english: languageE, language_spanish: languageS,
                  language_polish: languageP, willingness_to_pay: willpay, date: reqday, rating: rating,
                  time: time, minage: minage, maxage: maxage, status: "0")

end


# 200 Alte Anfragen erstellen (der letzten 90 Tage)

200.times do

  client = User.where(role: "client").sample

  sex = ["w", "w", "m"].sample
  education = false
  driverlicense = false
  languageG = false
  languageE = false
  languageS = false
  languageP = false
  willpay = rand(60..100)
  reqday  =  Date.today - rand(1..90)
  rating =  1
  time =     ["morning", "noon", "afternoon", "evening"].sample
  minage = 18
  maxage = 80

  # Wurde der Auftrag zugeteilt?
  assigned = [0,1,1,1,1].sample

  if assigned == 1
    ananny = Nanny.where(sex: sex).sample
    assigned_nanny = ananny.name
    status = 1
    # Schon bewertet?
    rated = rand(0..1)
    if rated == 1
      ratingdone = true
      ratingvalue = rand(1..5)
    else
      ratingdone = nil
      ratingvalue = nil
    end
  else
    assigned_nanny = nil
    ratingdone = nil
    ratingvalue = nil
    status = 3
  end

  Request.create!(name: client.name, sex: sex, education: education, driverlicense: driverlicense,
                  language_german: languageG, language_english: languageE, language_spanish: languageS,
                  language_polish: languageP, willingness_to_pay: willpay, date: reqday, rating: rating,
                  time: time, minage: minage, maxage: maxage, assigned_nanny: assigned_nanny,
                  ratingdone: ratingdone, ratingvalue: ratingvalue, status: status)


end

# 40 Alte Anfragen erstellen (der letzten 2 Wochen)

40.times do

  client = User.where(role: "client").sample

  sex = ["w", "w", "m"].sample
  education = false
  driverlicense = false
  languageG = false
  languageE = false
  languageS = false
  languageP = false
  willpay = rand(60..100)
  reqday  =  Date.today - rand(1..15)
  rating =  1
  time =     ["morning", "noon", "afternoon", "evening"].sample
  minage = 18
  maxage = 80

  # Wurde der Auftrag zugeteilt?
  assigned = [0,1,1,1,1].sample

  if assigned == 1
    ananny = Nanny.where(sex: sex).sample
    assigned_nanny = ananny.name
    status = 1
    # Schon bewertet?
    rated = rand(0..1)
    if rated == 1
      ratingdone = true
      ratingvalue = rand(1..5)
    else
      ratingdone = nil
      ratingvalue = nil
    end
  else
    assigned_nanny = nil
    ratingdone = nil
    ratingvalue = nil
    status = 3
  end

  Request.create!(name: client.name, sex: sex, education: education, driverlicense: driverlicense,
                  language_german: languageG, language_english: languageE, language_spanish: languageS,
                  language_polish: languageP, willingness_to_pay: willpay, date: reqday, rating: rating,
                  time: time, minage: minage, maxage: maxage, assigned_nanny: assigned_nanny,
                  ratingdone: ratingdone, ratingvalue: ratingvalue, status: status)


end


# 60 Alte Anfragen erstellen (des letzten Monats) nur für nanny1, nanny2 und nanny3

60.times do

  client = User.where(role: "client").sample

  sex = ["w", "w", "m"].sample
  education = false
  driverlicense = false
  languageG = false
  languageE = false
  languageS = false
  languageP = false
  willpay = rand(60..100)
  reqday  =  Date.today - rand(1..30)
  rating =  1
  time =     ["morning", "noon", "afternoon", "evening"].sample
  minage = 18
  maxage = 80

  # Wurde der Auftrag zugeteilt?
  assigned = 1

  if assigned == 1
    assigned_nanny = ["nanny1", "nanny2", "nanny3"].sample
    status = 1
    # Schon bewertet?
    rated = rand(0..1)
    if rated == 1
      ratingdone = true
      ratingvalue = rand(1..5)
    else
      ratingdone = nil
      ratingvalue = nil
    end
  else
    assigned_nanny = nil
    ratingdone = nil
    ratingvalue = nil
    status = 3
  end

  Request.create!(name: client.name, sex: sex, education: education, driverlicense: driverlicense,
                  language_german: languageG, language_english: languageE, language_spanish: languageS,
                  language_polish: languageP, willingness_to_pay: willpay, date: reqday, rating: rating,
                  time: time, minage: minage, maxage: maxage, assigned_nanny: assigned_nanny,
                  ratingdone: ratingdone, ratingvalue: ratingvalue, status: status)


end