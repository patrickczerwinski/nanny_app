class CreateAvailabilities < ActiveRecord::Migration
  def change
    create_table :availabilities do |t|
      t.string :nannyname
      t.string :date
      t.integer :morning
      t.integer :noon
      t.integer :afternoon
      t.integer :evening

      t.timestamps null: false
    end
  end
end
