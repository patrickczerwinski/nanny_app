class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.string :name
      t.string :sex
      t.integer :age
      t.boolean :education
      t.boolean :driverlicense
      t.boolean :language_german
      t.boolean :language_english
      t.boolean :language_spanish
      t.boolean :language_polish
      t.float :willingness_to_pay
      t.float :rating
      t.string :date
      t.string :time
      t.integer :minage
      t.integer :maxage
      t.string :assigned_nanny

      t.timestamps null: false
    end
  end
end
