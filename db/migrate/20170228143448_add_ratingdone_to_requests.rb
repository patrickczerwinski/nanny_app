class AddRatingdoneToRequests < ActiveRecord::Migration
  def change
    add_column :requests, :ratingdone, :boolean
    add_column :requests, :ratingvalue, :integer
    add_column :requests, :status, :integer
  end
end
