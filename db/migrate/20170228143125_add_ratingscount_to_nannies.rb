class AddRatingscountToNannies < ActiveRecord::Migration
  def change
    add_column :nannies, :ratingscount, :integer
    add_column :nannies, :minage, :integer
    add_column :nannies, :maxage, :integer
  end
end
