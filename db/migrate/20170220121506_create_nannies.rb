class CreateNannies < ActiveRecord::Migration
  def change
    create_table :nannies do |t|
      t.string :name
      t.string :sex
      t.integer :age
      t.boolean :education
      t.boolean :driverlicense
      t.boolean :language_german
      t.boolean :language_english
      t.boolean :language_spanish
      t.boolean :language_polish
      t.float :price
      t.float :rating

      t.timestamps null: false
    end
  end
end
